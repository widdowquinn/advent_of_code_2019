{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Day 3\n",
    "\n",
    "- [https://adventofcode.com/2019/day/3](https://adventofcode.com/2019/day/3)\n",
    "\n",
    "```text\n",
    "--- Day 3: Crossed Wires ---\n",
    "\n",
    "The gravity assist was successful, and you're well on your way to the Venus refuelling station. During the rush back on Earth, the fuel management system wasn't completely installed, so that's next on the priority list.\n",
    "\n",
    "Opening the front panel reveals a jumble of wires. Specifically, two wires are connected to a central port and extend outward on a grid. You trace the path each wire takes as it leaves the central port, one wire per line of text (your puzzle input).\n",
    "\n",
    "The wires twist and turn, but the two wires occasionally cross paths. To fix the circuit, you need to find the intersection point closest to the central port. Because the wires are on a grid, use the Manhattan distance for this measurement. While the wires do technically cross right at the central port where they both start, this point does not count, nor does a wire count as crossing with itself.\n",
    "\n",
    "For example, if the first wire's path is R8,U5,L5,D3, then starting from the central port (o), it goes right 8, up 5, left 5, and finally down 3:\n",
    "\n",
    "...........\n",
    "...........\n",
    "...........\n",
    "....+----+.\n",
    "....|....|.\n",
    "....|....|.\n",
    "....|....|.\n",
    ".........|.\n",
    ".o-------+.\n",
    "...........\n",
    "\n",
    "Then, if the second wire's path is U7,R6,D4,L4, it goes up 7, right 6, down 4, and left 4:\n",
    "\n",
    "...........\n",
    ".+-----+...\n",
    ".|.....|...\n",
    ".|..+--X-+.\n",
    ".|..|..|.|.\n",
    ".|.-X--+.|.\n",
    ".|..|....|.\n",
    ".|.......|.\n",
    ".o-------+.\n",
    "...........\n",
    "\n",
    "These wires cross at two locations (marked X), but the lower-left one is closer to the central port: its distance is 3 + 3 = 6.\n",
    "\n",
    "Here are a few more examples:\n",
    "\n",
    "    R75,D30,R83,U83,L12,D49,R71,U7,L72\n",
    "    U62,R66,U55,R34,D71,R55,D58,R83 = distance 159\n",
    "    R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\n",
    "    U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = distance 135\n",
    "\n",
    "What is the Manhattan distance from the central port to the closest intersection?\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Python imports\n",
    "from aoc2019 import run_tests, wire_get_shortest_overlap_delay, wire_get_shortest_overlap_distance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Inputs and expected outputs\n",
    "expected = [([\"R8,U5,L5,D3\", \"U7,R6,D4,L4\"], 6),\n",
    "            ([\"R75,D30,R83,U83,L12,D49,R71,U7,L72\", \"U62,R66,U55,R34,D71,R55,D58,R83\"], 159),\n",
    "            ([\"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\", \"U98,R91,D20,R16,D67,R40,U7,R15,U6,R7\"], 135)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Run tests\n",
    "run_tests(wire_get_shortest_overlap_distance, expected)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge Part 1\n",
    "\n",
    "Puzzle input is obtained from [https://adventofcode.com/2019/day/3](https://adventofcode.com/2019/day/3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load challenge data\n",
    "with open(\"day03.txt\", \"r\") as ifh:\n",
    "    paths = [_.strip() for _ in ifh]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "217"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Solve challenge\n",
    "wire_get_shortest_overlap_distance(paths)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Challenge Part 2\n",
    "\n",
    "```text\n",
    "--- Part Two ---\n",
    "\n",
    "It turns out that this circuit is very timing-sensitive; you actually need to minimize the signal delay.\n",
    "\n",
    "To do this, calculate the number of steps each wire takes to reach each intersection; choose the intersection where the sum of both wires' steps is lowest. If a wire visits a position on the grid multiple times, use the steps value from the first time it visits that position when calculating the total value of a specific intersection.\n",
    "\n",
    "The number of steps a wire takes is the total number of grid squares the wire has entered to get to that location, including the intersection being considered. Again consider the example from above:\n",
    "\n",
    "...........\n",
    ".+-----+...\n",
    ".|.....|...\n",
    ".|..+--X-+.\n",
    ".|..|..|.|.\n",
    ".|.-X--+.|.\n",
    ".|..|....|.\n",
    ".|.......|.\n",
    ".o-------+.\n",
    "...........\n",
    "\n",
    "In the above example, the intersection closest to the central port is reached after 8+5+5+2 = 20 steps by the first wire and 7+6+4+3 = 20 steps by the second wire for a total of 20+20 = 40 steps.\n",
    "\n",
    "However, the top-right intersection is better: the first wire takes only 8+5+2 = 15 and the second wire takes only 7+6+2 = 15, a total of 15+15 = 30 steps.\n",
    "\n",
    "Here are the best steps for the extra examples from above:\n",
    "\n",
    "    R75,D30,R83,U83,L12,D49,R71,U7,L72\n",
    "    U62,R66,U55,R34,D71,R55,D58,R83 = 610 steps\n",
    "    R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\n",
    "    U98,R91,D20,R16,D67,R40,U7,R15,U6,R7 = 410 steps\n",
    "\n",
    "What is the fewest combined steps the wires must take to reach an intersection?\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Tests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Inputs and expected outputs\n",
    "expected = [([\"R8,U5,L5,D3\", \"U7,R6,D4,L4\"], 30),\n",
    "            ([\"R75,D30,R83,U83,L12,D49,R71,U7,L72\", \"U62,R66,U55,R34,D71,R55,D58,R83\"], 610),\n",
    "            ([\"R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51\", \"U98,R91,D20,R16,D67,R40,U7,R15,U6,R7\"], 410)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Input: ['R8,U5,L5,D3', 'U7,R6,D4,L4']\n",
      "\tExpected: 30\n",
      "\tReturned: 30\n",
      "Input: ['R75,D30,R83,U83,L12,D49,R71,U7,L72', 'U62,R66,U55,R34,D71,R55,D58,R83']\n",
      "\tExpected: 610\n",
      "\tReturned: 610\n",
      "Input: ['R98,U47,R26,D63,R33,U87,L62,D20,R33,U53,R51', 'U98,R91,D20,R16,D67,R40,U7,R15,U6,R7']\n",
      "\tExpected: 410\n",
      "\tReturned: 410\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Run tests\n",
    "run_tests(wire_get_shortest_overlap_delay, expected)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "3454"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Solve challenge\n",
    "wire_get_shortest_overlap_delay(paths)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
