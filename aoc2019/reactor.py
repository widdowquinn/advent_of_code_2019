# -*- coding: utf-8 -*-
"""Module for day 14 of Advent of Code 2019."""

import math

from typing import List, Tuple

import networkx as nx

from networkx.classes.reportviews import NodeView


class Reactor:

    """Describes a reactor for fuel production.

    The system of reactions is represented as a directed
    graph with weighted edges. The reaction:

    7 A, 1 B => 1 C

    has three nodes and two edges:

    A -7-> C; B -1->C

    edge weights represent stoichiometry of the reaction.

    The reaction:

    10 ORE => 10 A

    has two nodes and one edge:

    ORE -10-> A(10)

    edge weights represent stoichiometry of the reaction,
    and node labels indicate minimum values to be produced.
    """

    def __init__(self) -> None:
        """Initialise reactor."""
        self._reactor = nx.DiGraph()

    def add_reaction(
        self, substrates: List[Tuple[str, int]], product: Tuple[str, int]
    ) -> None:
        """Add reaction to graph.

        :param substrates:  substrates to the reaction
        :param product:  product of the reaction

        Add nodes/edges to graph. For each substrate, add:

        substrate -int-> product(int)

        where edge labels are input substrate stoichiometries, and
        product node label is output quantity of product.
        """
        # Add product info
        # multiples is the minimum multiple of a chemical required
        # so if a node has 57 units, but the multiples is 10, we
        # need to ask for 60 units' worth of supply from the upstream
        # chemicals
        pname, pquant = product
        self._reactor.add_node(pname, required=0, multiples=pquant, visited=False)

        # Add edges
        for sname, stoich in substrates:
            self._reactor.add_node(sname, visited=False, required=0)
            # stoichiometry is relative amount of stoich/pquant
            self._reactor.add_edge(sname, pname, stoich=(stoich / pquant))

    def calculate_max_fuel(self, ore: float = 1e12, step: float = 1e10) -> int:
        """Return maximum amount of fuel that can be produced by available ore.

        :param ore:  available ore.
        """
        # Calculate how much ORE is required per unit FUEL
        unit_val = self.calculate_ore_requirement(1)

        # Calculate initial fuel estimate
        estimate = ore / unit_val
        requirement = self.calculate_ore_requirement(int(estimate))

        # Move requirement towards available ore value, gradient descent style
        while True:
            # print(math.log10(step), estimate)
            if requirement < ore:
                estimate += step
                requirement = self.calculate_ore_requirement(int(estimate))
                if requirement > ore:
                    if step == 1:
                        return int(estimate - 1)
                    step = step / 10
            elif requirement > ore:
                estimate -= step
                requirement = self.calculate_ore_requirement(int(estimate))
                if requirement < ore:
                    if step == 1:
                        return int(estimate)
                    step = step / 10
            elif requirement == ore:
                return int(estimate)

    def calculate_ore_requirement(self, requirement: int = 1) -> int:
        """Return the amount of ore required to make one unit of fuel.

        :param requirement:  required fuel amount

        We start at the fuel node, and set "required" to 1

        We then look at each input node, and the edge to get there
        from FUEL. If the edge has weight W, we require W units of the
        chemical. So we increment the "required" attribute of that node
        by W.

        Once FUEL is sorted, we then process the nodes we've seen.
        We start with any nodes that only have incoming edges and go
        through the same process, with a minor adjustment:
        We check the "required" attribute of the node against the
        "multiples" attribute and round up to the nearest multiple
        of "multiples" if necessary.
        We then look at all the input edges/nodes, and set "required"
        on each input node to the current node's "required" * W.
        Once all input nodes are updated, we set "visited" on the
        current node, and move to the next node we've already seen
        that has no output edges (outdegree zero).
        """
        # Initialise working set of nodes with FUEL and ORE
        self.working = ["FUEL", "ORE"]

        # Set required FUEL to 1
        self._reactor.nodes["FUEL"]["required"] = requirement
        # print(f"required fuel: {self._reactor.nodes['FUEL']['required']}")

        # Iterate over working set and get first node with zero
        # *visited* out-degree
        while len(self.working) > 1:
            for curnode in self.working:
                # print(f"Checking node {curnode}")
                if self.get_unvisited_outdegree(curnode) == 0:
                    # print(f"{curnode} has zero outdegree: work with this.")

                    # Find edges leading into this node. Add to working set
                    for from_node, _ in self._reactor.in_edges(curnode):
                        if (from_node not in self.working) and (
                            self._reactor.nodes[from_node]["visited"] is False
                        ):
                            # print(f"Adding {from_node} to working set")
                            self.working.append(from_node)

                        # Update requirements at input nodes
                        self._reactor.nodes[from_node]["required"] += int(
                            self._reactor.edges[(from_node, curnode)]["stoich"]
                            * self.get_actual_requirement(curnode)
                        )
                        # print(
                        #     f"Updated {from_node} requirements to "
                        #     f"{self._reactor.nodes[from_node]['required']}"
                        # )

                    # Mark current node as visited and remove from working set
                    # print(f"Marking {curnode} as visited, and removing")
                    self._reactor.nodes[curnode]["visited"] = True
                    self.working.remove(curnode)
                # print(f"Current working set now: {self.working}")

        # Calculate output, restore original system, then return
        output = self._reactor.nodes["ORE"]["required"]
        self._reactor = self._original.copy()
        return output

    def get_actual_requirement(self, curnode):
        """Return current node requuirement adjusted for multiples.

        :param curnode:  name of node
        """
        required_now = self._reactor.nodes[curnode]["required"]
        # print(curnode, required_now)
        remainder = required_now % self._reactor.nodes[curnode]["multiples"]
        if remainder == 0:
            return required_now
        # Round up to nearest multiple
        return (
            math.ceil(required_now / self._reactor.nodes[curnode]["multiples"])
            * self._reactor.nodes[curnode]["multiples"]
        )

    def get_unvisited_outdegree(self, curnode: str) -> int:
        """Return number of unvisited nodes with edges from node.

        :param curnode:  name of node

        FUEL node always returns 0
        """
        return len(
            [
                to_node
                for _, to_node in self._reactor.out_edges(curnode)
                if self._reactor.nodes[to_node]["visited"] is False
            ]
        )

    def parse_reactions(self, reactions: str) -> None:
        """Read description of reactions.

        :param reactions:  list of chemical reactions

        We expect a block of text with one reaction per line.
        """
        for line in [_.strip() for _ in reactions.split("\n") if len(_.strip())]:
            substrates, product = self.parse_equation(line)
            self.add_reaction(substrates, product)

        # Copy reaction system to storage
        self._original = self._reactor.copy()

    def parse_equation(
        self, reaction: str
    ) -> Tuple[List[Tuple[str, int]], Tuple[str, int]]:
        """Process text equation into substrates and products.

        :param reaction:  reaction equation

        Substrates and products are split by "=>".
        Substrates are separated by ",".
        Stoichiometry and chemical lablels are split by " ".
        """
        substrates, product = [_.strip() for _ in reaction.split("=>")]
        substrates = [self.parse_chemical(_.strip()) for _ in substrates.split(",")]
        product = self.parse_chemical(product)
        return substrates, product

    @staticmethod
    def parse_chemical(chemical: str) -> Tuple[str, int]:
        """Return tuple of chemical name and stoichiometry.

        :param chemical:  participant in a reaction

        Chemical input takes the form "<int> <str>" for stoichiometry
        and name
        """
        val, name = chemical.split(" ")
        return (name, int(val))

    @property
    def nodes(self) -> NodeView:
        """Return view onto self._reactor.nodes."""
        return self._reactor.nodes

    @property
    def system(self) -> nx.DiGraph:
        """Reaction system as directed graph."""
        return self._reactor


def draw_system(reactor: Reactor) -> None:
    """Draw the reactor system.

    :param reactor:  fuel reactor
    """
    pos = nx.spring_layout(reactor.system)
    nx.draw(reactor.system, pos, with_labels=True)
    edge_labels = {(u, v): d["stoich"] for u, v, d in reactor.system.edges(data=True)}
    nx.draw_networkx_edge_labels(reactor.system, pos, edge_labels=edge_labels)
