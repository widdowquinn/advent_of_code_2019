# -*- coding: utf-8 -*-
"""Module for day 20 of Advent of Code 2019."""

import string

from collections import defaultdict
from typing import Dict, Optional

import matplotlib.pyplot as plt
import networkx as nx

from networkx.classes.reportviews import NodeView


class WarpMaze:

    """Graph representation of a maze with warping portals."""

    def __init__(self, mazedesc: Optional[str] = None) -> None:
        """Instantiate maze.

        :param mazedesc:  string representation of maze and portals
        """
        self.entrance = None
        self.exit = None
        if mazedesc is not None:
            self.parse_maze(mazedesc)
        else:
            self.maze = nx.Graph()  # empty maze

    def add_position(self, xpos: int, ypos: int, char: str) -> None:
        """Add position to map.

        :param xpos:  x co-ordinate on map
        :param ypos:  y co-ordinate on map
        :param char:  character at position on map

        If the character is a period (`.`) the position is visitable.
        If the character is a letter, the position is part of a portal.
        """
        # Exclude whitespace and walls
        if char in string.whitespace or char == "#":
            return

        # Label for position
        posname = f"({xpos},{ypos})"

        # Add and decorate position if visitable or portal
        if char == ".":
            self.maze.add_node(posname)
            self.nodes[posname]["label"] = ""
            self.nodes[posname]["visitable"] = True  # is path
            self.nodes[posname]["portal"] = False  # is portal
            self.nodes[posname]["has_portal"] = False  # path with a portal
            self.nodes[posname]["portal_id"] = ""  # ID of portal for path
        elif char in string.ascii_uppercase:
            self.maze.add_node(posname)
            self.nodes[posname]["label"] = char  # part of portal name
            self.nodes[posname]["visitable"] = False
            self.nodes[posname]["portal"] = True
            self.nodes[posname]["has_portal"] = False
            self.nodes[posname]["portal_id"] = ""

        # Check neighbours and add edges if necessary
        for xnbor, ynbor in [
            (xpos - 1, ypos),
            (xpos + 1, ypos),
            (xpos, ypos - 1),
            (xpos, ypos + 1),
        ]:
            nborname = f"({xnbor},{ynbor})"
            if nborname in self.nodes:
                self.maze.add_edge(posname, nborname)

    def draw_maze(self):
        """Render maze as graph."""
        plt.figure(figsize=(8, 8))
        pos = nx.spring_layout(self.maze)
        nx.draw(self.maze, pos, with_labels=True)

    def find_shortest_path(self):
        """Return shortest path from entrance to exit.

        Entrance and exit are not counted as steps
        """
        path = nx.shortest_path(self.maze, self.entrance, self.exit)
        # subtract 1 from length as we don't count entrance
        return len(path) - 1

    def link_portals(self) -> None:
        """Connect nodes that are attached to the same portal.

        We assume that portals are labelled with unique pairs of letters.
        That is, if there is a portal `BC`, there cannot be a portal `CB`.
        If this is true, then we can start at any `end` portal node (which
        has a single edge) and walk to the first visitable location. That
        visitable location can then be labelled with the sorted portal
        letters under `portal_id`, and flagged with `has_portal`=True.
        """
        # Make dictionary of locations keyed by attached portal
        portaldict = defaultdict(list)  # type: Dict[str, list]
        for pos in self.portals:
            portalname = ""
            if self.maze.degree(pos) == 1:  # End of portal
                portalname += self.nodes[pos]["label"]
                nbor = next(self.maze.neighbors(pos))  # Next portal node
                portalname += self.nodes[nbor]["label"]
                portalname = "".join(sorted(portalname))
                path = [
                    _ for _ in self.maze.neighbors(nbor) if self.nodes[_]["visitable"]
                ][0]
                portaldict[portalname].append(path)

        # Link the portals and assign entrance/exit
        for key, vals in portaldict.items():
            if key == "AA":  # entrance
                self.entrance = vals[0]
            elif key == "ZZ":  # exit
                self.exit = vals[0]
            else:
                print(f"linking {tuple(vals)} (portal {key})")
                self.maze.add_edge(*tuple(vals))

    def parse_maze(self, mazedesc: str) -> None:
        """Parse maze from string.

        :param mazedesc:  string representation of maze and portals

        We have to be careful with parsing as the left/right/middle whitespace
        is meaningful in terms of placing portals.

        We also need to take care with the portals. Where we first find a character,
        we must wait to see if its partner path/letter is above, below, or to one
        side of the letter, before labelling the appropriate path.

        We initially treat characters as graph nodes, but later compress them to
        make the portals.
        """
        # Populate visitable paths of maze, and portals
        self.maze = nx.Graph()  # Fresh maze
        for row, line in enumerate(mazedesc.split("\n")):  # Iterate over rows
            for col, char in enumerate(line):  # Iterate over columns
                self.add_position(col, row, char)

        # Link portals
        self.link_portals()

    @property
    def nodes(self) -> NodeView:
        """Return NodeView onto maze."""
        return self.maze.nodes

    @property
    def portals(self):
        """Return nodes that are portals."""
        return [_ for _ in self.nodes if self.nodes[_]["portal"] is True]
