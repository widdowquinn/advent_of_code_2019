# -*- coding: utf-8 -*-
"""Module for day 15 of the Advent of Code 2019."""

from typing import Dict, List, Tuple, Union

import matplotlib.pyplot as plt
import matplotlib.animation as animation
import networkx as nx
import numpy as np

from .imachine import IntcodeMachine


class Position:

    """Represents a single co-ordinate position on the Map.

    Is updated by the Map as the RepairBot traverses the Map.
    """

    def __init__(self, xpos: int, ypos: int):
        """Initialise new position."""
        self.xpos = xpos
        self.ypos = ypos

        self.cleared = False  # True if all neighbours are assigned
        self.target = False  # True if this is the target location

    @property
    def name(self) -> str:
        """Return string version of location."""
        return f"({self.xpos},{self.ypos})"

    @property
    def neighbours(self) -> List[Tuple[int, int]]:
        """Return list of neighbour co-ordinates."""
        return [
            (self.xpos + 1, self.ypos),
            (self.xpos - 1, self.ypos),
            (self.xpos, self.ypos + 1),
            (self.xpos, self.ypos - 1),
        ]


class Map:

    """Represents the map explored by a RepairBot.

    It is gradually extended and updated as the RepairBot
    explores, being filled in by Position objects.

    It is a single representation that can return data as
    a np.array or an nx.Graph.
    """

    def __init__(self, bot) -> None:  # type: ignore
        """Initialise empty map."""
        # Dictionary of Position objects, keyed by Position
        # name: these can be visited by the bot
        self._positions = dict()  # type: Dict[Tuple[int, int], Position]
        # Dictionary of Position objects, keyed by Position
        # name: these are walls and cannot be visited by the bot
        self._walls = dict()  # type: Dict[str, Position]
        self._bot = bot

    def add_position(self, xpos, ypos) -> None:
        """Add Position (visitable) to Map.

        :param xpos:  x co-ordinate
        :param ypos:  y co-ordinate
        """
        self._positions[(xpos, ypos)] = Position(xpos, ypos)

    def add_wall(self, xpos, ypos):
        """Add Position (wall) to Map, annotated as wall.

        :param xpos:  x co-ordinate
        :param ypos:  y co-ordinate
        """
        self._walls[(xpos, ypos)] = Position(xpos, ypos)

    def available_moves(self, xpos: int, ypos: int) -> List[int]:
        """Return available moves from position.

        :param x:  x co-ordinate
        :param y:  y co-ordinate
        """
        # Have all neighbours been tried?
        if self._positions[(xpos, ypos)].cleared is True:
            return list()
        nbors = {
            (xpos, ypos - 1): 1,
            (xpos, ypos + 1): 2,
            (xpos - 1, ypos): 3,
            (xpos + 1, ypos): 4,
        }
        return [move for (pos, move) in nbors.items() if pos not in self.assigned]

    def is_cleared(self, xpos: int, ypos: int) -> None:
        """Check if position is cleared and update status.

        :param xpos:  x co-ordinate
        :param ypos:  y co-ordinate
        """
        # Have all neighbours been tried?
        if self._positions[(xpos, ypos)].cleared is False:
            nbors = [
                (xpos + 1, ypos),
                (xpos - 1, ypos),
                (xpos, ypos + 1),
                (xpos, ypos - 1),
            ]
            if len([_ for _ in nbors if _ in self.assigned]) == 4:
                self._positions[(xpos, ypos)].cleared = True

    def draw_array_map(self) -> None:
        """Draw array map."""
        plt.imshow(self.array_map)

    def draw_graph_map(self) -> None:
        """Draw graph map."""
        nx.draw(self.graph_map, with_labels=True)

    def nearest_uncleared_path(self, xpos: int, ypos: int) -> List[Tuple[int, int]]:
        """Return path to the closest uncleared map position from coords.

        :param x:  x co-ordinate
        :param y:  y co-ordinate
        """
        # Find paths to all uncleared locations and sort from short to long
        paths = list()  # type: List[Tuple[int, List[Tuple[int, int]]]]
        for pos in self.uncleared:
            path = nx.astar_path(self.graph_map, (xpos, ypos), pos)
            paths.append((len(path), path))
        return sorted(paths)[0][-1]

    def fewest_moves(self, xpos1, ypos1, xpos2, ypos2):
        """Return smallest number of moves between (xpos1, ypos1) and (xpos2, ypos2).

        :param xpos1:  x co-ordinate
        :param ypos1:  y co-ordinate
        :param xpos2:  x co-ordinate
        :param ypos2:  y co-ordinate
        """
        return nx.astar_path_length(self.graph_map, (xpos1, ypos1), (xpos2, ypos2))

    def visit(self, xpos: int, ypos: int, target: bool = False) -> None:
        """Visit the map at position (x, y).

        :param x:  x co-ordinate
        :param y:  y co-ordinate
        :param target:  True if this is the target location

        If the current (x,y) position does not exist, add it.
        Otherwise, check if we know what is at all co-ordinates.
        """
        if (xpos, ypos) not in self._positions:
            self.add_position(xpos, ypos)
        self[xpos, ypos].target = target

    def __getitem__(self, key: Tuple[int, int]) -> Position:
        """Return position with name `key` from Map."""
        return self._positions[key]

    @property
    def array_map(self) -> np.array:
        """Return Map as np.array.

        Elements of the array are keyed as:

        0: unexplored
        1: wall
        2: visited/pathway
        4: target
        5: bot

        The array size is determined live by the extremes of visited/walled areas.
        """
        # Get min/max x and y coords
        x_min, x_max, y_min, y_max = 0, 0, 0, 0
        for coll in (self._positions, self._walls):
            for pos in coll.values():  # type: ignore
                if pos.x < x_min:
                    x_min = pos.x
                elif pos.x > x_max:
                    x_max = pos.x
                if pos.y < y_min:
                    y_min = pos.y
                elif pos.y > y_max:
                    y_max = pos.y

        # Convert between bot and array co-ordinates
        x_offset, y_offset = -x_min, -y_min

        # Create new array to hold map
        y_size, x_size = (y_max - y_min + 1), (x_max - x_min + 1)
        map_array = np.zeros((y_size, x_size))

        # Add vwalls
        for pos in self._walls.values():
            map_array[pos.y + y_offset, pos.x + x_offset] = 1

        # Add visited positions
        for pos in self._positions.values():
            map_array[pos.y + y_offset, pos.x + x_offset] = 2

        # Add target position
        tgtpos = self.target
        if len(tgtpos) > 0:
            x_tgt, y_tgt = tgtpos
            map_array[y_tgt + y_offset, x_tgt + x_offset] = 4

        # Add bot position
        map_array[self._bot.y + y_offset, self._bot.x + x_offset] = 5

        # Return map array
        return map_array

    @property
    def graph_map(self) -> nx.Graph:
        """Return graph of known visitable positions."""
        map_graph = nx.Graph()  # undirected graph

        # Add nodes, edges for each known visitable positions
        for coords, pos in self._positions.items():
            for nbor in pos.neighbours:
                if nbor in self._positions:
                    map_graph.add_edge(coords, nbor)

        # Add current position, if necessary
        if self._bot.curpos not in map_graph.nodes():
            map_graph.add_node(self._bot.curpos)

        return map_graph

    @property
    def assigned(self) -> List[Tuple[int, int]]:
        """Return locations of visited positions or walls on map."""
        return self.visited + self.walls

    @property
    def cleared(self) -> List[Tuple[int, int]]:
        """Return locations of all cleared position on map."""
        return [(_.x, _.y) for _ in self._positions.values() if _.cleared]

    @property
    def target(self) -> Union[List, Tuple[int, int]]:
        """Return the location of the target position."""
        tgtpos = [(_.x, _.y) for _ in self._positions.values() if _.target]
        if len(tgtpos) > 0:
            return tgtpos[0]
        return []

    @property
    def uncleared(self) -> List[Tuple[int, int]]:
        """Return locations of all uncleared position on map."""
        return [(_.x, _.y) for _ in self._positions.values() if _.cleared is False]

    @property
    def visited(self) -> List[Tuple[int, int]]:
        """Return locations of visited positions on map."""
        return [(_.x, _.y) for _ in self._positions.values()]

    @property
    def walls(self) -> List[Tuple[int, int]]:
        """Return locations of walls on map."""
        return [(_.x, _.y) for _ in self._walls.values()]


class RepairBot(IntcodeMachine):

    """Implements a bot that can take instructions and return status."""

    def __init__(self, *args, **kwargs) -> None:
        """Set attributes unique to RepairBot."""
        super(RepairBot, self).__init__(*args, **kwargs)

        # Initial position of bot
        self.xpos = 0
        self.ypos = 0

        # Map of visited locations as undirected graph
        self.map = Map(self)
        self.map.visit(self.xpos, self.ypos)

    def available_moves(self) -> List[int]:
        """Return moves availabe to bot."""
        return self.map.available_moves(self.xpos, self.ypos)

    def explore(self, animate: bool = False) -> None:
        """Move around the map until no more moves are possible.

        :param animate:  if True, produce animated maze exploration

        We explore by:

        - find path to nearest uncleared known location
        - move along that path
        - when at the location, get the available moves
        - make the first available move
        """
        # Holds maps if animating
        if animate:
            self.explore_maps = [
                self.map.array_map.copy(),
            ]

        while len(self.map.uncleared) > 0:
            # Fond path to next uncleared known location
            path = self.map.nearest_uncleared_path(self.xpos, self.ypos)

            # Move along path
            moves = self.path_to_moves(path)
            for move in moves:
                self.trydir(move)

            # Get available moves
            moves = self.available_moves()
            # print(self.curpos, moves, len(self.visited), len(self.uncleared))
            if len(moves) > 0:
                self.trydir(moves[0])

            if animate:
                self.explore_maps.append(self.map.array_map.copy())

        if animate:
            fig = plt.figure(figsize=(4, 4))
            self.img = plt.imshow(
                self.explore_maps[0], interpolation="none", aspect="auto"
            )
            framecount = len(self.explore_maps)
            fps = 60  # frames per second of animation
            anim = animation.FuncAnimation(
                fig, self._animate, frames=framecount, interval=1000 / fps,  # in ms
            )
            anim.save("explore_map.mp4", fps=fps, extra_args=["-vcodec", "libx264"])

            # Don't show animation in notebook
            plt.close()

    def _animate(self, idx) -> List[np.array]:
        """Return screenshot for frame.

        :param idx:  index of screenshot
        """
        # if idx % 300 == 0:
        #     print(".",)
        img = plt.imshow(self.explore_maps[idx], interpolation="none", aspect="auto")
        return [img]

    def move(self, dirval: int, outval: int) -> None:
        """Move bot in specified direction.

        :param dirval:  direction to move.
        :param outval:  program output value (2 if target found)
        """
        old_x, old_y = self.xpos, self.ypos
        if dirval == 1:  # North
            self.ypos -= 1
        if dirval == 2:  # South
            self.ypos += 1
        if dirval == 3:  # West
            self.xpos -= 1
        if dirval == 4:  #  East
            self.xpos += 1
        target = outval == 2  # Is this position the target?
        self.map.visit(self.xpos, self.ypos, target)
        self.map.is_cleared(old_x, old_y)
        self.map.is_cleared(self.xpos, self.ypos)

    def nearest_uncleared_path(self) -> List[Tuple[int, int]]:
        """Return list of moves to nearest uncleared location."""
        return self.map.nearest_uncleared_path(self.xpos, self.ypos)

    @staticmethod
    def path_to_moves(path: List[Tuple[int, int]]) -> List[int]:
        """Return list of moves corresponding to path."""
        # First element of path is current position
        if len(path) == 1:  # Current position is not cleared
            return []
        x_1, y_1 = path.pop(0)  # Get first path location (current position)
        moves = list()  # type: List[int]
        while len(path) > 0:  # Iterate over path
            x_2, y_2 = path.pop(0)
            if y_2 - y_1 < 0:  #  North
                moves.append(1)
            elif y_2 - y_1 > 0:  # South
                moves.append(2)
            elif x_2 - x_1 < 0:  # West
                moves.append(3)
            elif x_2 - x_1 > 0:  # East
                moves.append(4)
            x_1, y_1 = x_2, y_2
        return moves

    def trydir(self, val: int) -> int:
        """Return result of attempt to move the bot.

        :param val:  direction to move

        Directions:

        1. North
        2. South
        3. West
        4. East

        Returns value and either moves or does not, depending
        on result:

        0. Hit a wall (position unchanged)
        1. Moved one step in direction (position changed)
        2. Moved one step in direction (position changed) and
           found the target.
        """
        output = next(self.outputs(val))
        # If output is 1 or 2, move the bot
        if output in (1, 2):  # Move the bot
            self.move(val, output)
        if output == 0:  #  Hit a wall
            self.wall(val)
        return output

    def wall(self, dirval: int) -> None:
        """Add wall to map in appropriate direction.

        :param dirval:  direction to move.
        :param outval:  program output value (2 if target found)
        """
        xpos, ypos = self.xpos, self.ypos
        if dirval == 1:  # North
            ypos -= 1
        if dirval == 2:  # South
            ypos += 1
        if dirval == 3:  # West
            xpos -= 1
        if dirval == 4:  #  East
            xpos += 1
        self.map.add_wall(xpos, ypos)
        self.map.is_cleared(self.xpos, self.ypos)

    @property
    def cleared(self) -> List[Tuple[int, int]]:
        """Return locations of cleared positions on map."""
        return self.map.cleared

    @property
    def uncleared(self) -> List[Tuple[int, int]]:
        """Return locations of uncleared positions on map."""
        return self.map.uncleared

    @property
    def visited(self) -> List[Tuple[int, int]]:
        """Return locations of visited positions on map."""
        return self.map.visited

    @property
    def curpos(self) -> Tuple[int, int]:
        """Return current bot co-ordinate."""
        return self.xpos, self.ypos
