# -*- coding: utf-8 -*-
"""Module for day 20 of Advent of Code 2019."""

import string

from collections import defaultdict
from typing import Dict, List, Optional, Tuple

import matplotlib.pyplot as plt
import networkx as nx

from networkx.classes.reportviews import NodeView


class MazeTemplate:

    """Graph representation of a recursive maze.

    This is the template maze that is reused to drawn the next level
    on each recursion.
    """

    def __init__(self, mazedesc: Optional[str] = None) -> None:
        """Instantiate template.

        :param mazedesc:  string description of maze.
        """
        self.entrance = None  # Entry node, always level 0
        self.exit = None  # Exit node, always level 0
        if mazedesc is None:
            self.maze = nx.Graph()  # Empty maze
        else:
            self.parse_maze(mazedesc)

    def add_position(self, xpos: int, ypos: int, char: str) -> None:
        """Add position to maze.

        :param xpos:  x co-ordinate on map
        :param ypos:  y co-ordinate on map
        :param char:  character at position on map

        If the character is a period (`.`) the position is visitable.
        If the character is a letter, the position is part of a portal.
        """
        # Exclude whitespace and walls
        if char in string.whitespace or char == "#":
            return

        # Label for position
        posname = (xpos, ypos)

        # Add and decorate position if visitable or portal
        if char == ".":
            self.maze.add_node(posname)
            self.nodes[posname]["label"] = ""
            self.nodes[posname]["visitable"] = True  # is path
            self.nodes[posname]["portal"] = False  # is portal
            self.nodes[posname]["has_portal"] = False  # path with a portal
            self.nodes[posname]["portal_id"] = ""  # ID of portal for path
        elif char in string.ascii_uppercase:
            self.maze.add_node(posname)
            self.nodes[posname]["label"] = char  # part of portal name
            self.nodes[posname]["visitable"] = False
            self.nodes[posname]["portal"] = True
            self.nodes[posname]["has_portal"] = False
            self.nodes[posname]["portal_id"] = ""

        # Check neighbours and add edges if necessary
        for xnbor, ynbor in [
            (xpos - 1, ypos),
            (xpos + 1, ypos),
            (xpos, ypos - 1),
            (xpos, ypos + 1),
        ]:
            nborname = (xnbor, ynbor)
            if nborname in self.nodes:
                self.maze.add_edge(posname, nborname)

    def copy_to_level(self, level: int):
        """Return a copy of the template graph at appropriate level.

        :param level:  maze level to construct from template

        Only level zero gets an entrance and exit.

        Level zero gets no outer portals.

        Outer portals are labelled by current level. Inner portals are
        labelled according to the level they lead to.
        """
        # Create new graph with mapping of template node names to new level
        mapping = {key: (key[0], key[1], level) for key in self.nodes}
        new_graph = nx.relabel_nodes(self.maze, mapping, copy=True)

        # Produce level-appropriate entrance/exit, and inner/outer portals
        if level == 0:
            new_entrance = tuple(list(self.entrance) + [0])  # type: ignore
            new_exit = tuple(list(self.exit) + [0])  # type: ignore
        else:
            new_entrance = new_exit = None  # type: ignore
        new_portalpaths = [tuple(list(_) + [level]) for _ in self.portalpaths]
        return new_graph, new_portalpaths, new_entrance, new_exit

    def draw_maze(self):
        """Render maze as graph."""
        plt.figure(figsize=(8, 8))
        pos = nx.spring_layout(self.maze)
        nx.draw(self.maze, pos, with_labels=True)

    def parse_maze(self, mazedesc: str) -> None:
        """Parse maze graph from string representation.

        :param mazedesc:  string representation of maze and portals

        Similarly to WarpMaze, we need to take care of whitespace as it is
        meaningful for placing portals.

        We take extra care with portals, as the entry and entrance locations
        apply only on the top level (level 0) of the recursive maze. Also,
        the inner portals link to the next lower level of the maze, and the
        outer portals link to the next higher level of the maze. Connecting
        levels is handled in the RecursiveMaze class.
        """
        # Populate visitable maze paths, and collect portals
        self.maze = nx.Graph()  # Fresh maze
        for row, line in enumerate(mazedesc.split("\n")):  # Iterate over rows
            for col, char in enumerate(line):  # Iterate over columns
                self.add_position(col, row, char)

        # Process portals
        self.process_portals()

    def process_portals(self) -> None:
        """Assign portals as inner or outer.

        Outer portals exist on the outer edges of the maze, and link to upper
        levels only. We identify these by having an extreme location for row
        or column. The remaining portals are inner portals and link to lower
        levels only.
        """
        #  Find min and max row and column values in template
        xvals, yvals = zip(*self.portals)
        xmin, ymin, xmax, ymax = min(xvals), min(yvals), max(xvals), max(yvals)

        # Dictionary of portals, keyed by portal name
        self.portaldict = defaultdict(list)  # type: Dict[str, List]

        # Identify portal ends with min/max values
        for portal in self.portals:
            # Identify inner/outer
            if (
                (portal[0] == xmin)
                or (portal[0] == xmax)
                or (portal[1] == ymin)
                or (portal[1] == ymax)
            ):  # outer portal
                portaltype = "outer"
            else:
                portaltype = "inner"

            # Label path next to portal
            portalname = ""
            if self.maze.degree(portal) == 1:  # End of portal
                portalname += self.nodes[portal]["label"]
                self.nodes[portal]["portaltype"] = portaltype
                nbor = next(self.maze.neighbors(portal))  # Next portal node
                portalname += self.nodes[nbor]["label"]
                portalname = "".join(sorted(portalname)) + f"_{portaltype}"
                self.nodes[nbor]["portaltype"] = portaltype
                path = [
                    _ for _ in self.maze.neighbors(nbor) if self.nodes[_]["visitable"]
                ][0]
                self.nodes[path]["portalpath"] = portalname
                self.portaldict[portalname].append(path)

            # Assign entrance and exit
            for key, vals in self.portaldict.items():
                if key.startswith("AA"):  # entrance
                    self.entrance = vals[0]
                elif key.startswith("ZZ"):  # exit
                    self.exit = vals[0]

    @property
    def inner_portals(self) -> NodeView:
        """Return NodeView onto maze."""
        return [_ for _ in self.portals if self.nodes[_]["portaltype"] == "inner"]

    @property
    def nodes(self) -> NodeView:
        """Return NodeView onto maze."""
        return self.maze.nodes

    @property
    def outer_portals(self) -> NodeView:
        """Return NodeView onto maze."""
        return [_ for _ in self.portals if self.nodes[_]["portaltype"] == "outer"]

    @property
    def portalpaths(self):
        """Return nodes that are portalpaths."""
        return [_ for _ in self.nodes if "portalpath" in self.nodes[_]]

    @property
    def portals(self):
        """Return nodes that are portals."""
        return [_ for _ in self.nodes if self.nodes[_]["portal"] is True]


class RecurseMaze:

    """Recursive maze made of at least one MazeTemplate."""

    def __init__(self, template: Optional[str] = None) -> None:
        """Instantiate maze template.

        :param template:  string description of maze.
        """
        self.entrance = None  # Entry node, always level 0
        self.exit = None  # Exit node, always level 0
        if template is None:
            self.template = nx.Graph()  # Empty maze
        else:
            self.template = MazeTemplate(template)

        # Initialise maze
        self.maze = nx.Graph()
        self.portalpaths = list()  # type: List[Tuple]

        # Create level zero
        self.initialise_maze()

    def draw_maze(self):
        """Render maze as graph."""
        plt.figure(figsize=(8, 8))
        pos = nx.spring_layout(self.maze)
        nx.draw(self.maze, pos, with_labels=True)

    def find_shortest_path_length(self) -> int:
        """Return shortest path length from entrance to exit."""
        while self.has_solution() is False:
            self.recurse_level()
        return nx.shortest_path_length(self.maze, self.entrance, self.exit)

    def has_solution(self) -> bool:
        """Return True if there is a path from entrance to exit."""
        return nx.has_path(self.maze, self.entrance, self.exit)

    def initialise_maze(self) -> None:
        """Use template to construct top level of maze.

        This is done by adding nodes from the template, amended to indicate
        their level.
        """
        # Build dictionary of maze levels
        self.levels = {0: self.template.copy_to_level(0)}

        # Add nodes, edges from level zero to the graph
        self.maze = nx.compose(self.maze, self.levels[0][0])
        self.portalpaths.extend(self.levels[0][1])
        self.entrance = self.levels[0][2]
        self.exit = self.levels[0][3]

    def linklevels(self, upper: int, lower: int) -> None:
        """Link from upper level to lower level by portals.

        :param upper:  upper level
        :param lower:  lower level

        Connect inner portals from upper level to outer portals of
        lower level.
        """
        # Get inner portals of upper level
        upper_portals = [
            _
            for _ in self.levels[upper][1]
            if self.nodes[_]["portalpath"].endswith("inner")
        ]
        # print(upper_portals)

        # Get outer portals of lower level
        lower_portals = [
            _
            for _ in self.levels[lower][1]
            if self.nodes[_]["portalpath"].endswith("outer")
        ]
        # print(lower_portals)

        # Make dictionary of portals by portal name
        portaldict = defaultdict(list)  # type: Dict[str, list]
        for portal in upper_portals + lower_portals:
            key = self.nodes[portal]["portalpath"][:2]
            portaldict[key].append(portal)

        # Link portals
        # print(portaldict)
        for key, vals in portaldict.items():
            if len(vals) == 2:  # ignore "entrance"/"exit"
                # print(f"adding edge: {vals}")
                self.maze.add_edge(vals[0], vals[1])

    def recurse_level(self) -> None:
        """Add next lower level to maze."""
        # Copy template to next level
        oldlevel = max(self.levels)
        nextlevel = oldlevel + 1
        self.levels[nextlevel] = self.template.copy_to_level(nextlevel)

        # Add nodes, edges from next level
        self.maze = nx.compose(self.maze, self.levels[nextlevel][0])
        self.portalpaths.extend(self.levels[nextlevel][1])

        # Link levels by portals
        self.linklevels(oldlevel, nextlevel)

    @property
    def nodes(self) -> NodeView:
        """Return NodeView onto maze."""
        return self.maze.nodes
