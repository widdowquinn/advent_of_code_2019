# -*- coding: utf-8 -*-
"""Module for Advent of Code 2019 solutions."""

from typing import Iterator, List, Tuple

import numpy as np


def asteroid_paths_pol(
    home: Tuple[int, int], asteroids: List[Tuple[int, int]]
) -> List[Tuple[float, float]]:
    """Return polar co-ordinates of each asteroid from the "home" asteroid.

    :param home:  Tuple[int, int] cartesian coordinate of home base
    :param asteroids:  List[Tuple[int, int]] cartesian coords of asteroids
    """
    return [get_polar_coords(home, asteroid) for asteroid in asteroids]


def cart_to_polar(x: int, y: int) -> Tuple[float, float]:
    """Return polar (len, angle) co-ordinates from cartesian.

    :param x:  int, x co-ordinate
    :param y:  int, y co-ordinate
    """
    r = np.sqrt(x ** 2 + y ** 2)
    t = np.arctan2(y, x)
    if t == 3.141592653589793:
        t = -3.141592653589793
    return (t, r)


def count_visible_asteroids(
    asteroids: List[Tuple[int, int]]
) -> Iterator[Tuple[int, Tuple[int, int]]]:
    """Return a list of the count of other asteroids visible from each asteroid.

    :param asteroids:  List[Tuple[int, int]], (x, y) co-ordinates of asteroids

    For each asteroid, find the unit vector to that asteroid, and the distance
    along that vector.

    An asteroid is visible if its unit vector is unique, or its distance is
    the shortest among the non-unique unit vectors
    """
    visible_count = list()
    for home in asteroids:
        visible = dict()  # type: ignore
        for away in asteroids:
            if home != away:
                xdiff = away[1] - home[1]
                ydiff = away[0] - home[0]
                vec = np.array([xdiff, ydiff])
                mag = np.linalg.norm(vec, ord=1)
                univec = tuple(vec / mag)
                dist = np.sqrt(xdiff ** 2 + ydiff ** 2)
                if univec not in visible:
                    visible[univec] = (away, dist)
                else:
                    if dist < visible[univec][1]:
                        visible[univec] = (away, dist)
        visible_count.append(len(visible))
    return zip(visible_count, asteroids)


def destruction_order(
    home: Tuple[int, int], asteroids: List[Tuple[int, int]]
) -> List[Tuple[int, int]]:
    """Return asteroid (x, y) co-ordinates in order of destruction.

    :param home:  Tuple[int, int] cartesian coordinate of home (laser) base
    :param asteroids: List[Tuple[int, int]] of asteroid coords

    Starts with laser vertical (angle = 3.141592653589793)
    """
    destroyed = list()  # holds asteroid co-ordinates in order of destruction

    # Get polar co-ordinates for each asteroid and sort by angle
    data = sorted(list(zip(asteroid_paths_pol(home, asteroids), asteroids)))

    # Remove home base from list
    data.remove(((0.0, 0.0), home))

    # destroy asteroids in order
    last_angle = None
    while len(data):
        to_remove = list()
        for (pol, cart) in data:
            if pol[0] == last_angle:  # was hidden
                # print(f"{cart} was hidden")
                last_angle = pol[0]
                continue
            else:
                # first asteroid at this angle: destroy
                # print(f"{cart} was destroyed")
                destroyed.append(cart)
                to_remove.append((pol, cart))
                last_angle = pol[0]
        # reset data
        for (pol, cart) in to_remove:
            data.remove((pol, cart))
        last_angle = None
    return destroyed


def find_best_station(mapdesc: List[str]) -> Tuple:
    """Return location of best monitoring station and count of visible asteroids.

    :param mapdesc:  List[str], multi-line string
    """
    visibles = count_visible_asteroids(locate_asteroids(mapdesc))
    return sorted(list(visibles))[-1]


def get_polar_coords(
    home: Tuple[int, int], away: Tuple[int, int]
) -> Tuple[float, float]:
    """Return polar coordinates of away relative to home.

    :param home:  Tuple[int, int] cartesian coordinate of home base
    :param away:  Tuple[int, int] cartesian coordinate of away asteroid
    """
    xdiff = away[1] - home[1]
    ydiff = home[0] - away[0]  # reversed because map coords flipped vertically
    return cart_to_polar(xdiff, ydiff)


def locate_asteroids(mapdesc: List[str]) -> List[Tuple[int, int]]:
    """Return cartesian (x, y) co-ordinates of asteroids on the map.

    :param mapdesc:  List[str], multi-line string

    In the map description, "." indicates an empty space, and "#" indicates
    an asteroid.

    Returns a list of (x, y) co-ordinates with (0, 0) at top left
    """
    asteroids = []
    for ypos, line in enumerate([_.strip() for _ in mapdesc if len(_.strip())]):
        for xpos, char in enumerate(line.strip()):
            if char == "#":
                asteroids.append((xpos, ypos))
    return asteroids
