# -*- coding: utf-8 -*-
"""Module for Space Image Format problems in Advent of Code 2019."""

from typing import List

import numpy as np


def compose_sif(arr: np.array) -> np.array:
    """Return a 2D array of final composed image.

    :param arr:  np.array, input N-dimensional array
    """
    _, rows, columns = arr.shape
    outarr = np.full((rows, columns), 2)  #  "transparent" array
    for row in range(rows):
        for col in range(columns):
            stack = list(arr[:, row, col])
            val = stack.pop(0)
            while val == 2:
                val = stack.pop(0)
            outarr[row, col] = val
    return outarr


def count_elements(arr: np.array, val: int) -> int:
    """Return count of elements in arr (2D array) equal to val.

    :param arr:  np.array (2D) of ints
    :param val:  element to search for
    """
    return sum(sum(np.equal(arr, val)))  # type: ignore


def digits_to_data(data: str) -> List[int]:
    """Return list of digits from passed string.

    :param data:  str, input image data
    """
    return [int(_) for _ in data]


def get_fewest_zero_layer(arr: np.array) -> np.array:
    """Return layer of a multidimensional array having fewest zero cells.

    :param arr:  np.array for analysis
    """
    zero_counts = []  # List of (layer_index, zero_count) tuples
    for _ in range(arr.shape[0]):
        zero_counts.append((count_elements(arr[_, :, :], 0), _))
    zero_counts = sorted(zero_counts)
    return arr[zero_counts[0][1]]


def parse_sif(data: str, height: int, width: int) -> np.array:
    """Parse a string of digits into a multilayer image matrix.

    :param data:  str, input data
    :param height:  int, height of image
    :param width:  int, width of image

    Returns a numpy multidimensional array composed of the input data
    """
    # Find height of the image
    layers = int(len(data) / width / height)

    # Create 1D array from digits
    arr = np.array([digits_to_data(data)], np.int32)

    # Reshape into multidimensional array
    return arr.reshape(layers, height, width)  # pylint: disable=too-many-function-args
