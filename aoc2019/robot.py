# -*- coding: utf-8 -*-
"""Module for Advent of Code 2019 day 11."""

from typing import Set, Tuple

import numpy as np

from .imachine import IntcodeMachine


class Robot:

    """Hull-painting robot, day 11 of Advent of Code 2019."""

    def __init__(
        self, curpos: Tuple[int, int] = (0, 0), curdir: Tuple[int, int] = (-1, 0)
    ) -> None:
        """Initialise Robot state.

        :param curpos:  Tuple[int, int] current position in cartesian coordinates
        :param curdir:  Tuple[int, int] current direction robot faces
        :param program:  List[int] Intcode program for robot

        Current direction self.curdir can be added to self.curpos to get
        next location of robot.
        """
        self.curpos = curpos  # current robot position
        self.curdir = curdir  # which way the robot faces
        self._ptr = 0  #  initial pointer for program, may be updated
        self._visited = set()  # type: Set[Tuple] # log visited panels

    def detect_current_panel(self, surface: np.array) -> int:
        """Return 0 (black), 1 (white) for current panel colour.

        :param surface:  the current surface on which the robot works

        If no surface is available, returns 0. Otherwise polls colour
        at self.curpos on passed surface.
        """
        return int(surface[self.curpos[0], self.curpos[1]])

    def move(self) -> None:
        """Update current position by one step."""
        self.curpos = (self.curpos[0] + self.curdir[0], self.curpos[1] + self.curdir[1])
        # print(f"Updated position: {self.curpos}")

    def paint_current_panel(self, surface: np.array) -> np.array:
        """Paint the panel at robot position and return painted surface.

        The robot detects the current panel colour and receives 1 or 0
        depending on the colour. This value is used as input to the
        robot's program. The first output from running that program
        determines the colour that the panel is painted: 0 (black),
        1 (white). The second output determines the way the robot moves:
        0 (90 degrees left) or 1 (90 degrees right).

        The robot then moves forward one step, updating its current
        position.
        """
        # print(f"Painting panel {self.curpos}")
        # Log current position
        self._visited.add(self.curpos)

        # Get panel colour at current position on surface
        panel_colour = self.detect_current_panel(surface)
        # print(f"Old panel colour: {panel_colour}")

        # Run program: get new colour for panel
        new_colour = next(self._imachine.outputs(invals=panel_colour))
        rotval = next(self._imachine.outputs(invals=panel_colour))
        # print(f"New colour: {new_colour}")
        # print(f"Rotation value: {rotval}")

        # Paint panel in new colour
        surface[self.curpos[0], self.curpos[1]] = new_colour
        # print(surface)

        # Update direction
        self.update_direction(rotval)

        # Move one step
        self.move()

        # Return surface
        return surface

    def program(self, program: str) -> None:
        """(Re)program the robot.

        :param program:  List[int] Intcode program for robot
        """
        self._imachine = IntcodeMachine(program, memsize=1000)

    def start(self, surface: np.array) -> None:
        """Start the robot painting.

        :param surface:  surface to be painted
        """
        # print("Starting robot")
        try:
            while True:
                surface = self.paint_current_panel(surface)
        except StopIteration:
            return surface

    def update_direction(self, dirn: int) -> None:
        """Rotate robot 90deg left or right.

        :param dirn:  0 or 1, indicating left or right.

        Updates self.curdir according to turn direction.
        """
        leftdir = {
            (-1, 0): (0, -1),  # up -> left
            (0, -1): (1, 0),  # left -> down
            (1, 0): (0, 1),  # down -> right
            (0, 1): (-1, 0),  # right -> up
        }
        rightdir = {
            (-1, 0): (0, 1),  # up -> right
            (0, 1): (1, 0),  # right -> down
            (1, 0): (0, -1),  # down -> left
            (0, -1): (-1, 0),  # left -> up
        }
        if dirn == 0:
            rotdir = leftdir
        else:
            rotdir = rightdir
        self.curdir = rotdir[self.curdir]
        # print(f"Rotated to {self.curdir}")
