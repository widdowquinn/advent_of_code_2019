# -*- coding: utf-8 -*-
"""Module for day 16 of Advent of Code 2019."""

import numpy as np

from tqdm.notebook import tqdm


def fft(
    signal: str,
    pattern: np.array,
    phases: int = 1,
    repeat: int = 1,
    offset: bool = False,
    verbose: bool = False,
) -> str:
    """Return FFT-processed signal.

    :param signal:  input signal
    :param pattern;  pattern for FFT processing
    :param phases:  number of phases of FFT
    :param repeat:  number of times to repeat input signal for processing
    :param offset:  if True, use first seven digits of signal to find return value
    :param verbose:  if True, show output at each phase
    """
    # Target region for offset output
    target = int(signal[:7])

    # Convert signal to array
    signal = process_signal(signal, repeat)

    # Extend pattern to same length as signal
    # First phase truncates the left digit of resulting pattern
    initpattern = extend_pattern(pattern, len(signal) + 1)[1:]
    # Subsequent phases use the whole pattern
    pattern = extend_pattern(pattern, len(signal))

    # Set threshold for speedier operation
    # Steps after half the signal length of steps are all positive sums
    thresh = int(len(signal) / 2)

    # Subsequent steps
    if offset is True:  # Offset points to second half of output
        # Uses the observation that the second half of the final output (which contains
        # our target location) depends only on the second half of the signal input.
        # Each digit in that output is the last digit of the sum of the digits in the
        # second half of the input.
        # So we need only use the last half of the signal input. To get the target
        # value, we need only start at the target position.
        # We can set an initial sum of the truncated signal (target onwards) and then,
        # in each phase, we subtract each digit in turn from the truncated signal
        # in turn to get the next output from that phase. This is much faster than
        # summing thousands of digits.
        trunc_signal = signal[target:]
        for _ in tqdm(range(phases)):
            value = np.sum(trunc_signal)
            output = [
                value % 10,
            ]
            for idx in range(len(trunc_signal)):
                value -= trunc_signal[idx]
                output.append(value % 10)
            trunc_signal = np.array(output, np.int32)
            if verbose:
                print(_, output)
    else:  # Normal operation
        for _ in tqdm(range(phases)):
            # Calculate output
            output = []
            # Step 1
            output.append(calculate_output(signal, initpattern))
            # For the first half of the output we do the multiplication
            for idx in range(1, thresh):  # Remaining steps
                step = idx + 1
                step_pattern = pattern.repeat(step)[: len(signal) + 1][1:]
                output.append(calculate_output(signal, step_pattern))
            # For the second half of the output, we can use the same observation
            # as for the offset=True case
            trunc_signal = signal[thresh:]
            value = np.sum(trunc_signal)
            output.append(int(str(value)[-1]))
            for idx in range(len(trunc_signal) - 1):
                value -= trunc_signal[idx]
                output.append(value % 10)
            signal = np.array(output, np.int32)
            if verbose:
                print(output)

    # Return result as string
    outstr = "".join([str(_) for _ in output])
    return outstr[:8]


def calculate_output(signal: np.array, pattern: np.array) -> int:
    """Returns last digit of the sum of the elements in the result."""
    # get locations of +/-1 in step pattern
    pos_sum = np.sum(signal[pattern == 1])
    neg_sum = np.sum(signal[pattern == -1])
    return abs(pos_sum - neg_sum) % 10


def extend_pattern(pattern: np.array, length: int) -> np.array:
    """Return pattern repeated to given length.

    :param pattern:  array to be repeated
    :param length:  length of array to return
    """
    while len(pattern) < length:
        pattern = np.append(pattern, pattern)
    return pattern[:length]


def process_signal(signal: str, repeat: int) -> np.array:
    """Return array built from signal.

    :param signal:  input signal
    :param repeat:  number of repeats of input signal
    """
    signal = signal * repeat
    chars = [_ for _ in signal.strip()]
    return np.array([int(_) for _ in chars], np.int32)
