# -*- coding: utf-8 -*-
"""Module for day 22 of Advent of Code 2019."""

from typing import Optional

import numpy as np


class Deck:

    """Holds and shuffles a deck of space-cards."""

    def __init__(self, n_cards: int, verbose: bool = False) -> None:
        """Instantiate deck with n_cards cards.
        
        :param n_cards:  number of cards in deck
        :param verbose:  if true give verbose output
        """
        self.cards = np.arange(n_cards)
        self.verbose = verbose

    def cut(self, n_cards: int) -> None:
        """Cut deck at given number of cards.

        :param n_cards:  point in deck to cut
        """
        if self.verbose:
            print(f"cut {n_cards}")
        part1 = list(self.cards[:n_cards])
        part2 = list(self.cards[n_cards:])
        self.cards = np.array(part2 + part1, np.int32)

    def deal(self, increment: Optional[int] = None) -> None:
        """Deal cards out from top to bottom.

        :param increment:  increment to use

        Here, the increment is the gap between the cards in the new
        dealt order, not the gap between dealt cards in the original
        deck.
        """
        if increment is None:
            self.cards = self.cards[::-1]
            if self.verbose:
                print(f"deal into new stack")
            return
        if self.verbose:
            print(f"deal with increment {increment}")
        offset = increment * np.arange(len(self))
        mod = offset % len(self)
        mapping = mod
        newcards = np.zeros(len(self), np.int32)
        for oldpos, newpos in enumerate(mapping):
            newcards[newpos] = self.cards[oldpos]
        self.cards = newcards

    def find_card(self, card: int) -> int:
        """Return location of card with passed number.

        :param card:  card number
        """
        return int(np.where(self.cards == card)[0])

    def run_instructions(self, instructions: str) -> None:
        """Run a set of text instructions on the deck.

        :param instructions:  text instructions
        """
        for instr in [_.strip() for _ in instructions]:
            words = instr.split()
            if words[0] == "cut":
                val = int(words[-1])
                self.cut(val)
            elif words[0] == "deal":
                if words[-1] == "stack":
                    self.deal()
                else:
                    self.deal(int(words[-1]))

    def __len__(self) -> int:
        """Return size of card deck."""
        return len(self.cards)
