# -*- coding: utf-8 -*-
"""Module for day 21 of Advent of Code 2019."""

from typing import List

from .imachine import IntcodeMachine


class SpringDroid(IntcodeMachine):

    """Implements a jumping SpringDroid.

    SpringDroid understands springscript.
    SpringDroid has memory of 15 commands.
    Springscript has two registers that take Booleans:

    - T (temporary value)
    - J (jump)

    If J is True at end of program, droid jumps.
    SpringDroid has four sensors: A, B, C, D. These sense
    regions at distances from the droid. A is close, D is far
    away. Values are stored in registered A, B, C, D. For
    ground, the register is True, for a hole it is False.

    Springscript has three instructions:

    - AND X Y: if X, Y both True, set Y True (set Y False otherwise)
    - OR X Y: if X or Y True, set Y True (set Y False otherwise)
    - NOT X Y: if X False, set Y True (set Y False otherwise)

    Y must be a writable register (TJ), X can be either read-only (ABCD)
    or TJ.
    """

    def __init__(self, *args, **kwargs) -> None:
        """Set attributes unique to SpringDroid."""
        super(SpringDroid, self).__init__(*args, **kwargs)

    @staticmethod
    def parse_springscript(program: str) -> List[int]:
        """Return list of springscript instructions.

        :param program:  string of springscript instructions

        Strips left whitespace, then converts ascii to integers
        """
        return [ord(_) for _ in program.lstrip()]

    def get_prompt(self) -> str:
        """Get prompt for instructions."""
        # Get and return prompt
        prompt = []  # type: List[str]
        char = next(self.outputs())
        while char is not None:
            prompt += chr(char)
            char = next(self.outputs())
        return "".join(prompt)

    def run_springscript(self, program: str) -> int:
        """Run springscript program."""
        intcode = self.parse_springscript(program)
        result = list(self.outputs(intcode))
        print("".join([chr(_) for _ in result[:-1]]))
        return result[-1]
