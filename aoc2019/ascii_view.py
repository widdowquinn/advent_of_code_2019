# -*- coding: utf-8 -*-
"""Module for day 17 of Advent of Code 2019."""

from typing import List, Optional, Tuple, Union

import matplotlib.pyplot as plt
import numpy as np

from .imachine import IntcodeMachine


class View(IntcodeMachine):

    """Intcode machine that views scaffold and vacuum robot."""

    def acquire_image(self, invals: Optional[Union[List[int], int]] = None) -> None:
        """Acquire current image and store.

        :param inval:  input value to program
        """
        self.image = list(self.outputs(invals))

    def find_intersections(self) -> List[Tuple[int, int]]:
        """Return locations on current image where scaffolds intersect.

        Scaffolds have value 35 and intersect when the North,
        South, East, and West positions are also scaffolds.
        """
        # Get x, y locations of scaffolds
        locs = np.where(self.array_image == 35)
        scaf_set = set(zip(locs[0], locs[1]))

        # Find x, y locations where NSEW are already present
        intersections = []
        for y_val, x_val in scaf_set:
            if (y_val - 1, x_val) not in scaf_set:  # N
                continue
            if (y_val + 1, x_val) not in scaf_set:  # S
                continue
            if (y_val, x_val + 1) not in scaf_set:  # E
                continue
            if (y_val, x_val - 1) not in scaf_set:  # W
                continue
            intersections.append((y_val, x_val))
        return intersections

    @property
    def ascii_image(self) -> str:
        """Return current image as ASCII string."""
        return "".join([chr(_) for _ in self.image])

    @property
    def array_image(self) -> np.array:
        """Return current image as numpy array."""
        rows = []
        row = []  # type: List[int]

        # magic number from running code
        for _ in self.image[:2157]:
            if _ == 10:
                if len(row) > 0:
                    rows.append(row)
                row = []
            else:
                row.append(_)
        return np.array(rows, np.int32)

    @property
    def intersection_checksum(self) -> int:
        """Return checksum of intersection locations."""
        return sum([x * y for x, y in self.find_intersections()])

    @property
    def plot_image(self):
        """Return matplotlib plot of current image."""
        plt.imshow(self.array_image)
