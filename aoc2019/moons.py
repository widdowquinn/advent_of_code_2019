# -*- coding: utf-8 -*-
"""Module supporting solutions ofr day 12 of Advent of Code 2019."""

from itertools import combinations
from typing import List, Tuple

import numpy as np


class Moon:

    """Represents a moon, with velocity, potential energy, and stochastic updating."""

    def __init__(
        self,
        name: str,
        pos: Tuple[int, int, int] = (0, 0, 0),
        vel: Tuple[int, int, int] = (0, 0, 0),
    ) -> None:
        """Initialise moon with name and position in 3D space.

        :param name:  name of moon
        :param pos:  moon position in (x, y, z) coordinates
        :param vel:  moon velocity in (x, y, z) coordinates
        """
        self.name = name
        self.pos = np.array(pos, np.int32)
        self.vel = np.array(vel, np.int32)

    def update_position(self) -> None:
        """Update moon position in response to current velocity."""
        self.pos += self.vel

    def update_velocity(self, moon) -> None:
        """Update moon velocity in response to adjacent moon.

        :param moon:  influencing moon
        """
        self.vel += np.sign(moon.pos - self.pos)

    def __str__(self) -> str:
        """Return string representation of moon."""
        outstr = [
            f"{self.name}",
            f"x={self.x}, y={self.y}, z={self.z}",
            f"v_x={self.v_x}, v_y={self.v_y}, v_z={self.v_z}",
            f"potential energy: {self.pot_energy}",
            f"kinetic energy: {self.kin_energy}",
            f"total energy: {self.total_energy}",
            f"stste: {self.state}",
        ]
        return "\n\t".join(outstr)

    @property
    def x(self) -> int:
        """Return x-coordinate of moon."""
        return self.pos[0]

    @property
    def y(self) -> int:
        """Return y-coordinate of moon."""
        return self.pos[1]

    @property
    def z(self) -> int:
        """Return z-coordinate of moon."""
        return self.pos[2]

    @property
    def v_x(self) -> int:
        """Return x-velocity of moon."""
        return self.vel[0]

    @property
    def v_y(self) -> int:
        """Return y-velocity of moon."""
        return self.vel[1]

    @property
    def v_z(self) -> int:
        """Return z-velocity of moon."""
        return self.vel[2]

    @property
    def pot_energy(self) -> int:
        """Return potential energy of moon.

        This is sum of absolute values of x, y, z co-ords
        """
        return np.sum(np.abs(self.pos))

    @property
    def kin_energy(self) -> int:
        """Return kinetic energy of moon.

        This is sum of absolute values of x, y, z velocities
        """
        return np.sum(np.abs(self.vel))

    @property
    def total_energy(self) -> int:
        """Return total energy of moon.

        This is product of potential and kinetic energies
        """
        return self.kin_energy * self.pot_energy

    @property
    def state(self) -> str:
        """Return state of moon as a string."""
        return ",".join(
            [str(_) for _ in [self.x, self.y, self.z, self.v_x, self.v_y, self.v_z]]
        )


class MoonSystem:

    """Represents a system of moons that interact through gravity."""

    def __init__(self, name: str, moons: List[Moon], time: int = 0) -> None:
        """Initialise moon system with some moons.

        :param name:  name of the system
        :param moons:  Moon objects in the system
        :param time:  timestamp of system
        """
        self.name = name
        self.moons = moons
        self.time = time

    def find_period(self):
        """Return periodic time to repeat state in all three dimensions.

        The dimensions are orthogonal to/independent of each other, so can
        be considered individually. The three solutions can then be
        combined to obtain a solution in three dimensions, as the
        periodicity must be a multiple of the LCM
        """
        # Each dimension gets a list so we can recover index, and a
        # set for fast testing of whether the state was seen before
        x_states, x_stateset = list(), set()
        y_states, y_stateset = list(), set()
        z_states, z_stateset = list(), set()
        # Find repeat period in each dimension
        periods = [None, None, None]
        while any([_ is None for _ in periods]):
            self.update_system()
            x_state, y_state, z_state = list(), list(), list()
            for _ in self.moons:  # Get specific state in each dimension
                x_state += [_.x, _.v_x]
                y_state += [_.y, _.v_y]
                z_state += [_.z, _.v_z]
            # Check states against those already seen and, if there's a repeat,
            # this is our period
            for state, states, stateset, p_idx in [
                (x_state, x_states, x_stateset, 0),
                (y_state, y_states, y_stateset, 1),
                (z_state, z_states, z_stateset, 2),
            ]:
                # Only check dimensions we don't have a period for
                if periods[p_idx] is None:
                    state = tuple(state)
                    if state in stateset:  # test against set, not list, for speed
                        periods[p_idx] = self.time - states.index(state) - 1
                    states.append(state)
                    stateset.add(state)
        return lowest_common_multiple(periods)

    def update_system(self, steps=1) -> None:
        """Update moon system by a number of timesteps.

        :param steps:  number of timesteps to update

        At each step, the moons are drawn to each other (pairwise)
        by gravity. This modifies each moon's gravity one step in
        each of x, y, z co-ordinates closer to each other.
        """
        for _ in range(steps):
            # Apply gravity
            # Consider each unique pair combination of moons, in turn
            for moon1, moon2 in list(combinations(self.moons, 2)):
                moon1.update_velocity(moon2)
                moon2.update_velocity(moon1)
            # Update positions
            for moon in self.moons:
                moon.update_position()
            self.time += 1

    def __str__(self) -> str:
        """Return string summary of system."""
        outstr = [f"System: {self.name} (t={self.time})", "Moons:"]
        for moon in self.moons:
            outstr.append(f"\t{moon}")
        outstr.append(f"Total Energy in system: {self.total_energy}")
        outstr.append(f"State: {self.state}")
        return "\n".join(outstr)

    @property
    def total_energy(self) -> int:
        """Return total energy in system.

        This is the sum of total energies for each moon in the system.
        """
        return sum(_.total_energy for _ in self.moons)

    @property
    def state(self) -> str:
        """Return state of system as string."""
        return ",".join([_.state for _ in self.moons])


def lowest_common_multiple(vals: List[int]) -> int:
    """Return lowest common multiple of three values.

    :param vals:  values
    """
    vals = sorted(vals)  # largest value at end
    lcm = lcm_test = vals.pop()  # get largest value
    while len(vals) > 0:
        test = vals.pop()
        while True:
            if lcm_test % test == 0:  # test is a factor, this is an lcm of values seen
                lcm = lcm_test  # update lcm
                break
            lcm_test += lcm  # try next multiple of lcm
    return lcm
