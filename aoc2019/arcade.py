# -*- coding: utf-8 -*-
"""Module for day13 of Advent of Code 2019."""

from typing import List, Optional, Union

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation

from .imachine import IntcodeMachine


class ArcadeGame(IntcodeMachine):

    """Intcode arcade machine."""

    def clear_score(self) -> None:
        """Set score to zero."""
        self.score = 0

    def clear_screen(self) -> None:
        """Clear screen for game."""
        # Clear screen
        self.screen = np.zeros((26, 42), np.int32)

    def update_screen(self, inputs: Optional[Union[List[int], int]] = None) -> None:
        """Update array representing screen state."""
        # fill screen with pixels
        pixel = list()  # type: List[int]

        for opt in self.outputs(inputs):
            if opt is None:  # We need more input
                return
            if len(pixel) == 0:  # first of three outputs is instruction
                score = False
                if opt == -1:  # report score
                    score = True
            pixel.append(opt)
            if len(pixel) == 3:
                if score is False:
                    self.screen[pixel[1], pixel[0]] = pixel[2]
                else:
                    self.score = pixel[2]
                pixel = list()

    def free_play(self) -> None:
        """Set game for free play.

        Poke program position 0 to 2
        """
        self.poke(0, 2)

    def play(self, inputs: Optional[Union[List[int], int]] = None):
        """Play game by providing joystick movement commands.

        :param inputs:  joystick inputs

        Joystick inputs are -1 (left), 0 (neutral), 1 (right)
        """
        self.clear_screen()
        self.clear_score()
        self.update_screen(inputs)

    def win(self, animate: bool = False) -> np.array:
        """Win game by moving paddle to ball location.

        :param animate:  if True, produce animated game
        """
        self.clear_score()
        self.clear_screen()
        self.update_screen()  # start game

        # Holds screenshots if animating
        self.screenshots = [
            self.screen.copy(),
        ]

        # Move paddle towards ball
        while self.block_count > 0:
            if animate:
                self.screenshots.append(self.screen.copy())
            if self.pos_ball < self.pos_paddle:
                move = -1
            elif self.pos_ball > self.pos_paddle:
                move = 1
            else:
                move = 0
            self.update_screen(
                [move,]
            )
        self.update_screen()
        if animate:
            self.screenshots.append(self.screen.copy())

        if animate:
            # print(len(self.screenshots))
            fig = plt.figure(figsize=(4.6, 2.6))
            self.img = plt.imshow(
                self.screenshots[0], interpolation="none", aspect="auto"
            )
            framecount = len(self.screenshots)
            fps = 60  # frames per second
            anim = animation.FuncAnimation(
                fig, self._animate, frames=framecount, interval=1000 / fps,  # in ms
            )
            anim.save("win_game.mp4", fps=fps, extra_args=["-vcodec", "libx264"])
            anim.save("win_game.gif", dpi=80, writer="imagemagick", fps=fps)

        return self.screen

    def _animate(self, idx) -> List[np.array]:
        """Return screenshot for frame.

        :param idx:  index of screenshot
        """
        # if idx % 300 == 0:
        #     print(".",)
        self.img.set_array(self.screenshots[idx])
        return [self.img]

    @property
    def pos_ball(self) -> int:
        """Return x position of ball.

        There should be only one ball (value 4)
        """
        return np.where(self.screen == 4)[1][0]

    @property
    def pos_paddle(self) -> int:
        """Return x position of paddle.

        There should be onlyone paddle (value 3)
        """
        return np.where(self.screen == 3)[1][0]

    @property
    def block_count(self) -> int:
        """Return number of blocks on screen."""
        return sum(sum(self.screen == 2))  # type: ignore
