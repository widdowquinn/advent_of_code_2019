# -*- coding: utf-8 -*-
"""Module providing IntcodeMachine class."""

from typing import Generator, List, NamedTuple, Optional, Union


class Parameter(NamedTuple):

    """Holds parameter/mode pairs for an instruction/opcode."""

    param: int  # paramter value
    mode: int  # mode of the parameter

    def __repr__(self) -> str:
        """Return string representation."""
        return f"Parameter(val={self.param}, mode={self.mode})"

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.param}[{self.mode}]"


class Instruction(NamedTuple):

    """Holds an Intcode instruction, separated into opcode, and modes for parameters."""

    opcode: int  # opcode (defines instruction/function)
    params: List[Parameter]  # Parameters (and modes) for opcode

    def __repr__(self) -> str:
        """Return string representation."""
        return f"Instruction(opcode={self.opcode:02d}, parameters={[str(_) for _ in self.params]}"

    def __str__(self) -> str:
        """Return string representation."""
        return f"{self.opcode:02d}: {[str(_) for _ in self.params]}"


class IntcodeMachine:

    """Provides IntcodeMachine, a class to interpret and run Intcode.

    This is prepared for Advent of Code 2019. It's a refactoring of
    the original (eventually unwieldy) code into OO form.
    """

    def __init__(
        self,
        program: str = "",
        ptr: int = 0,
        relbase: int = 0,
        memsize: int = 0,
        verbose: bool = False,
    ) -> None:
        """Initialise machine.

        :param program:  intcode program as comma-separated string
        :param ptr:  pointer to current location on program
        :param relbase:  relative base of positions
        :param memsize:  number of extra zeros to add to program for memory
        :param verbose:  report on progress
        """
        #  Parse program and add memory
        self.parse_program(program)
        self.add_memory(memsize)
        self._orig_program = self.program[:]

        # Initialise state
        self.reset(ptr, relbase)

        # Housekeeping for object
        self.verbose = verbose
        self.initialise_opcodes()

    def add_memory(self, memsize: int) -> None:
        """Add working memory to current Intcode program.

        :param memsize: number of extra zeros to add to program for memory
        """
        self.program += [0] * memsize

    def _get_next_instruction(self) -> Instruction:
        """Return next instruction from program.

        Instructions have at least one digit (the opcode). Where there are more
        than two digits, the opcode is the rightmost pair.

        Moving left from the opcode, digits define parameter modes. Where there is not a
        parameter mode, it is assumed to be "0".
        """
        opn = str(self.program[self.ptr])  # operation and modes
        opcode = int(opn[-2:])  # last two digits are opcode

        # Parse modes
        modes = [int(_) for _ in opn[:-2][::-1]]  # get stated modes (in order)
        modes += [0] * (self.opcode_nargs[opcode] - len(modes))  # extend with zeros

        # Get parameters as list of Parameters
        paramvals = self.program[
            self.ptr + 1 : self.ptr + self.opcode_nargs[opcode] + 1
        ]
        params = [Parameter(_[0], _[1]) for _ in zip(paramvals, modes)]

        # Increment pointer to next instruction
        self.ptr += self.opcode_nargs[opcode] + 1

        return Instruction(opcode, params)  # type: ignore

    def _get_param_val(self, param: Parameter, output: bool = False) -> int:
        """Return value to be used by an intcode opcode, modified by parameter mode.

        :param param:  parameter
        :param output:  treat the parameter as output (i.e. ignore mode 1)

        The value in param.param is modified by what is at param.mode:

        - mode 0: position mode, return value at program[param]
        - mode 1: immediate mode, return param
        - mode 2: relative mode, return value at program[param + relbase]
        """
        if param.mode == 1 or (output is True and param.mode == 0):
            return param.param
        if param.mode == 2:
            if output is True:
                return param.param + self.relbase  # output gets location
            return self.program[param.param + self.relbase]  # non-output gets value
        return self.program[param.param]  # mode 0, not output

    def initialise_opcodes(self) -> None:
        """Set up opcode distribution data."""
        # Accepted opcode values
        self.opcodes = [1, 2, 3, 4, 5, 6, 7, 8, 9, 99]  # type: List[int]

        # Number of arguments for each opcode
        self.opcode_nargs = {
            1: 3,
            2: 3,
            3: 1,
            4: 1,
            5: 2,
            6: 2,
            7: 3,
            8: 3,
            9: 1,
            99: 0,
        }

        # Function for each opcode
        self.opcode_fn = {
            1: self.opcode_01,
            2: self.opcode_02,
            3: self.opcode_03,
            4: self.opcode_04,
            5: self.opcode_05,
            6: self.opcode_06,
            7: self.opcode_07,
            8: self.opcode_08,
            9: self.opcode_09,
            99: self.opcode_99,
        }

    def instructions(self) -> Generator:
        """Yield all instructions in the current program.

        :param inval:  input value to program
        """
        while True:
            instr = self._get_next_instruction()
            if instr.opcode == 99:
                yield instr
                break
            yield instr

    def opcode_01(self, params: List[Parameter]) -> None:
        """Execute ADD: p_1 + p_2, place at p_3.

        :param params:  parameters and modes for input.
        """
        p_1, p_2 = (self._get_param_val(_) for _ in params[:2])
        p_3 = self._get_param_val(params[2], output=True)
        self.program[p_3] = p_1 + p_2
        if self.verbose:
            print(
                f"opcode01: ADD: {p_1} + {p_2} -> program[{p_3}]; ({self.program[p_3]})"
            )

    def opcode_02(self, params: List[Parameter]) -> None:
        """Execute MUL: p_1 + p_2, place at p_3.

        :param params:  parameters and modes for input.
        """
        p_1, p_2 = (self._get_param_val(_) for _ in params[:2])
        p_3 = self._get_param_val(params[2], output=True)
        self.program[p_3] = p_1 * p_2
        if self.verbose:
            print(
                f"opcode02: MUL: {p_1} * {p_2} -> program[{p_3}]; ({self.program[p_3]})"
            )

    def opcode_03(self, params: List[Parameter]) -> None:
        """Place next input value at pointer.

        :param params:  paramater and mode for input.
        """
        nextval = self.invals.pop(0)  # left-most input value
        p_1 = self._get_param_val(params[0], output=True)
        self.program[p_1] = nextval
        if self.verbose:
            print(f"opcode03: IPT: {nextval} -> program[{p_1}]; ({self.program[p_1]})")

    def opcode_04(self, params: List[Parameter]) -> None:
        """Output value at pointer.

        :param params:  parameter and mode for input
        """
        p_1 = self._get_param_val(params[0])
        self.output = p_1
        if self.verbose:
            print(f"opcode04: OPT: {self.output}")

    def opcode_05(self, params: List[Parameter]) -> None:
        """Set pointer to param2 if param1 is non-zero.

        :param params:  parameter and mode for input
        """
        p_1, p_2 = (self._get_param_val(_) for _ in params[:2])
        if p_1 != 0:  #  move pointer
            self.ptr = max(p_2, 0)
        if self.verbose:
            print(f"opcode05: JIT: if {p_1} != 0: {p_2} -> ptr; ({self.ptr})")

    def opcode_06(self, params: List[Parameter]) -> None:
        """Set pointer to param2 if param1 is zero.

        :param params:  parameter and mode for input
        """
        p_1, p_2 = (self._get_param_val(_) for _ in params[:2])
        if p_1 == 0:  #  move pointer
            self.ptr = max(p_2, 0)
        if self.verbose:
            print(f"opcode06: JIF: if {p_1} == 0: {p_2} -> ptr; ({self.ptr})")

    def opcode_07(self, params: List[Parameter]) -> None:
        """Store 1 at param3 if param1 < param2.

        :param params:  parameter and mode for input
        """
        p_1, p_2 = (self._get_param_val(_) for _ in params[:2])
        p_3 = self._get_param_val(params[2], output=True)
        if p_1 < p_2:
            self.program[p_3] = 1
        else:
            self.program[p_3] = 0
        if self.verbose:
            print(
                f"opcode07: LT_: if {p_1} < {p_2}: 1 -> program[{p_3}]; "
                f"else 0 -> program[{p_3}]; ({self.program[p_3]})"
            )

    def opcode_08(self, params: List[Parameter]) -> None:
        """Store 1 at param3 if param1 == param2.

        :param params:  parameter and mode for input
        """
        p_1, p_2 = (self._get_param_val(_) for _ in params[:2])
        p_3 = self._get_param_val(params[2], output=True)
        if p_1 == p_2:
            self.program[p_3] = 1
        else:
            self.program[p_3] = 0
        if self.verbose:
            print(
                f"opcode08: EQ_: if {p_1} == {p_2}: 1 -> program[{p_3}]; "
                f"else 0 -> program[{p_3}]; ({self.program[p_3]})"
            )

    def opcode_09(self, params: List[Parameter]) -> None:
        """Adjust relative base for IntcodeMachine.

        :param params:  parameter and mode for input
        """
        p_1 = self._get_param_val(params[0])
        self.relbase += p_1
        if self.verbose:
            print(f"opcode09: REL: {p_1} -> relbase; ({self.relbase})")

    def opcode_99(self, params) -> None:
        """Halt Intcode execution."""
        if self.verbose:
            print(f"opcode99: HLT: halt operation. {params}")

    def outputs(self, invals: Union[List[int], int, None] = None) -> Generator:
        """Run current Intcode program until next output, input request, or halt.

        :param inval:  input value to program

        If there is output (including Halt), returns an int; if an input is
        required, returns None.
        """
        self._parse_inputs(invals)
        while True:
            ptr = self.ptr
            if self.waiting is None:  # type: ignore  # Not waiting for next input
                instr = self._get_next_instruction()
            else:
                instr = self.waiting  # type: ignore
            if self.verbose:
                print(f"{ptr:04d}: {instr}, invals: {self.invals}")
            try:
                self._run_instruction(instr)
            except IndexError:  #  Do we need more input
                if instr.opcode == 3:  # we need more input
                    # self.ptr -= 1  # step back one
                    self.waiting = (
                        instr  # We're now waiting for input, hold this instruction
                    )
                    yield None  # Pause to wait for more input
            self.waiting = None  # type: ignore  # Reset waiting state
            if instr.opcode == 4:
                yield self.output
            if instr.opcode == 99:
                break

    def _parse_inputs(self, invals: Optional[Union[List[int], int]] = None) -> None:
        """Process input values for program.

        :param invals:  input values for program
        """
        if invals is None:
            return
        if isinstance(invals, int):
            self.invals = [
                invals,
            ]
        else:
            self.invals = invals[:]

    def parse_program(self, program: str) -> None:
        """Set program state as list of tokens, parsed from string.

        :param program:  intcode program as comma-separated string
        """
        self.program = [int(_.strip()) for _ in program.split(",") if len(_.strip())]

    def poke(self, ptr: int, val: int) -> None:
        """Poke program value at ptr.

        :param ptr:  pointer/address in program
        :param val:  value to poke at address
        """
        self.program[ptr] = val
        if self.verbose:
            print(f"POKE: {val} -> program[{ptr}]")

    def reset(self, ptr: int = 0, relbase: int = 0, hard_reset: bool = False) -> None:
        """Reset pointer and relative base. Empty inputs/outputs.

        :param ptr:  pointer position on program
        :param relbase:  relative base for position offsets
        :param hard_reset:  if True, reset original progrem
        """
        self.ptr = ptr
        self.relbase = relbase
        self.invals = list()
        self.output = None  # type: ignore
        self.waiting = None  # type: ignore  # will hold instruction while we wait for input
        if hard_reset:
            self.program = self._orig_program[:]

    def run(self, invals: Optional[Union[List[int], int]] = None) -> int:
        """Run current intcode program. Return an output only when halted.

        :param invals:  input value to program
        """
        self._parse_inputs(invals)
        while True:
            ptr = self.ptr
            instr = self._get_next_instruction()
            if self.verbose:
                print(f"{ptr:04d}: {instr}; invals: {self.invals}")
            self._run_instruction(instr)
            if instr.opcode == 99:
                if self.output is None:
                    self.output = self.program[0]
                return self.output

    def _run_instruction(self, instr: Instruction) -> None:
        """Run a single instruction with the current program and state.

        :param instr:  instruction to be run
        """
        self.opcode_fn[instr.opcode](instr.params)  # type: ignore

    def __repr__(self):
        """Return representation of IntcodeMachine."""
        progstr = ", ".join([str(_) for _ in self.program[:5]])
        if len(self.program) > 5:
            progstr += ", ..."
        return (
            f"IntcodeMachine(program=[{progstr}], programlen={len(self.program)}, "
            f"ptr={self.ptr}, relbase={self.relbase})"
        )
