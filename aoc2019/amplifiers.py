# -*- coding: utf-8 -*-
"""Module for Amplifier solutions to Advent of Code 2019."""

from .imachine import IntcodeMachine


class Amplifier:

    """Wrapper around an Intcode machine. Prefixes input with a phase."""

    def __init__(self, program: str, phase: int, verbose: bool = False) -> None:
        """Initialise amplifier.

        :param program:  Intcode program
        :param phase:  phase input
        :param verbose:  report imachine state
        """
        self.phase = phase
        self.verbose = verbose
        self.imachine = IntcodeMachine(program)
        self.semaphore = False

    def run(self, inval: int) -> int:
        """Run Intcode machine with input value (and phase).

        :param inval:  input value to IntcodeMachine
        """
        if self.semaphore is False:
            self.semaphore = True
            return next(self.imachine.outputs(invals=[self.phase, inval]))
        return next(self.imachine.outputs(invals=inval))


class AmplifierChain:

    """Chain of Amplifier objects in series."""

    def __init__(self, program: str, phases: str, verbose: bool = False) -> None:
        """Instantiate a series of Amplifiers in series.

        :param program:  Intcode program
        :param phases:  phases for the amplifiers
        :param verbose:  report imachine state
        """
        phasevals = [int(_.strip()) for _ in phases.split(",")]
        self.loop = [Amplifier(program, phase, verbose) for phase in phasevals]

    def run(self, inval: int) -> int:
        """Run each Amplifier in series and return output.

        :param inval:  input value to IntcodeMachine

        Amplifiers are run in turn. The first Amplifier in self.loop
        takes inval as input; the next takes the output of the first
        amplifier, and so on.
        """
        for amp in self.loop:
            inval = amp.run(inval)
        return inval


class AmplifierFeedback:

    """Feedback loop of Amplifier objects."""

    def __init__(self, program: str, phases: str, verbose: bool = False) -> None:
        """Instantiate a series of Amplifiers in series.

        :param program:  Intcode program
        :param phases:  phases for the amplifiers
        :param verbose:  report imachine state
        """
        phasevals = [int(_.strip()) for _ in phases.split(",")]
        self.loop = [Amplifier(program, phase, verbose) for phase in phasevals]

    def run(self, inval: int) -> int:
        """Run each Amplifier in series and return output.

        :param inval:  input value to IntcodeMachine

        Amplifiers are run in turn. The first Amplifier in self.loop
        takes inval as input; the next takes the output of the first
        amplifier, and so on.
        """
        try:
            while True:
                for amp in self.loop:
                    inval = amp.run(inval)
        except StopIteration:
            return inval
