# -*- coding: utf-8 -*-
"""Module containing support for Advent of Code 2019.

See: https://adventofcode.com/2019
"""

from itertools import permutations
from math import floor
from typing import Callable, Generator, List, Tuple

import numpy as np

from .imachine import IntcodeMachine


def find_noun_verb(program: str, target: int) -> int:
    """Identify pairs of verb/noun values that give the desired result.

    :param program:  Intcode program
    :param target:  target output

    Iterates over combinations of verb, noun values to identify which
    gives the correct target value, and returns 100 * noun + verb.

    Returns zero if no verb, noun combination found
    """
    for verb, noun in permutations(list(range(100)) * 2, 2):
        imachine = IntcodeMachine(program)
        imachine.poke(1, noun)
        imachine.poke(2, verb)
        imachine.reset()
        output = imachine.run()
        if output == target:
            return 100 * noun + verb
    return 0


def fix_1202(intcode: List[int]) -> List[int]:
    """Restore Intcode program after 1202 error.

    :param intcode:  List[int], Intcode program

    Replace first two positions of Intcode program with [12, 2]
    """
    return [intcode[0]] + [12, 2] + intcode[3:]


def fix_error(intcode: List[int], noun: int, verb: int) -> List[int]:
    """Restore Intcode program after verb, noun error.

    :param intcode:  List[int], Intcode program
    :param noun:  int, value to correct program
    :param verb:  int, value to correct program

    Replace first two positions of Intcode program with [verb, noun]
    """
    return [intcode[0]] + [noun, verb] + intcode[3:]


def get_fuel_for_all_modules(masses: List[int]) -> int:
    """Return total fuel for all modules, accounting also for fuel mass.

    :param masses:  List[int], masses for each module

    Solution to Advent of Code 2019 Day 1, part 2
    """
    return sum([(get_total_mass(modmass) - modmass) for modmass in masses])


def get_total_mass(mass: int) -> int:
    """Return total mass of module + fuel to lift fuel and module.

    :param masses:  int, mass to be lifted

    Recursively calculates, from initial module mass, the total weight
    of the module and the fuel required to lift the module (and the
    fuel required to lift the module).
    """
    extra_fuel = get_required_fuel(mass)
    if extra_fuel > 0:  # more fuel needed, calculate again
        return mass + get_total_mass(extra_fuel)
    # no extra fuel needed, this is our total mass
    return mass


def get_required_fuel(mass: int) -> int:
    """Return fuel required to launch module, based on mass.

    :param mass:  int, module mass

    Solution to Advent of Code 2019 Day 1, part 1
    """
    return floor(mass / 3) - 2


def intcode_run(intcode: List[int]) -> int:
    """Run Intcode program.

    :param intcode:  List[int], Intcode program

    Run the Intcode program and return the value in position 0
    """
    for opcode in intcode_split(intcode):
        intcode = opcode_operate(opcode, intcode)
    return intcode[0]


def intcode_split(intcode: List[int]) -> Generator:
    """Split Intcode into 4-position Opcode chunks and return.

    :param intcode:  List[int], an Intcode program

    Generator yielding four-member List[int] Opcode instructions.
    """
    for idx in range(0, len(intcode), 4):
        opcode = intcode[idx : idx + 4]
        if opcode[0] == 99:  # End program, don't report this or subsequent opcodes
            return
        yield opcode


def opcode_operate(opcode: List[int], intcode: List[int]) -> List[int]:
    """Apply Opcode to Intcode program and return new Intcode program.

    :param opcode:  List[int], 4-member Opcode
    :param intcode:  List[int], Intcode program
    """
    instr = opcode[0]  # instruction
    addr1, addr2, addr3 = opcode[1:]  # registers
    if instr == 1:  # Add values and store
        intcode[addr3] = intcode[addr1] + intcode[addr2]
    if instr == 2:  # Multiply values and store
        intcode[addr3] = intcode[addr1] * intcode[addr2]
    return intcode


def pwd_test_venus_range(func: Callable, minval: int, maxval: int) -> List[int]:
    """Return list of passwords in range (minval, maxval) that pass test.

    :param func:  Callable, the password test function
    :param minval:  int, lower inclusive limit
    :param maxval:  int, upper inclusive limit

    Checks all passwords in range [minval, maxval] to see if they pass
    theh venus test and returns a list of those that do.
    """
    return [_ for _ in range(minval, maxval + 1) if func(_)]


def pwd_venus_test(val: int) -> bool:
    """Return True if value meets Venus password criteria.

    :param val:  int, password

    Returns true if val has six digits, the digits are ascending,
    and there is at least one pair of identical adjacent digits.
    """
    pwd = str(val)
    if len(pwd) != 6:  # not exactly six digits
        return False
    diffs = np.diff([int(_) for _ in pwd])
    if np.any(diffs < 0):  # non-ascending digits
        return False
    if not np.any(diffs == 0):  # no adjacent pairs
        return False
    return True


def pwd_venus_test_revised(val: int) -> bool:
    """Return True if value meets revised Venus password criteria.

    :param val:  int, password

    Returns true if val has six digits, the digits are ascending,
    and there is at least one pair of identical adjacent digits.
    Additionally, a pair of identical adjacent digits must be
    only a pair, not part of a larger repeated sequence.
    """
    pwd = str(val)
    if len(pwd) != 6:  # not exactly six digits
        return False
    diffs = np.diff([int(_) for _ in pwd])
    if np.any(diffs < 0):  # non-ascending digits
        return False
    if not np.any(diffs == 0):  # no adjacent pairs
        return False
    # Check for exact pairs
    potpair = bool(diffs[0] == 0)
    for idx in range(1, 5):
        if diffs[idx] != 0:
            if potpair is True:
                return True
            potpair = False
        if diffs[idx] == 0:
            if potpair is True:
                potpair = False
            if potpair is False and diffs[idx - 1] != 0:
                potpair = True
    return potpair


def run_tests(func: Callable, expected: List[Tuple], verbose: bool = True) -> bool:
    """Return true if tests pass.

    :param func:  Callable function solving the day's challenge
    :param expected:  List of Tuples, where the first item is the input, and the second
                      item is the expected output
    :param verbose:  bool, if True, interim results are print()ed to stdout
    """
    retvals = []
    for indata, outval in expected:
        if type(indata) in (list, tuple):
            funcout = func(*indata)
        else:
            funcout = func(indata)
        if verbose:
            print(f"Input: {indata}")
            print(f"\tExpected: {outval}")
            print(f"\tReturned: {funcout}")
            retvals.append(funcout == outval)
    return all(retvals)


def wire_get_shortest_overlap_delay(
    wires: List[str], start: Tuple[int, int] = (0, 0)
) -> int:
    """Return the shortest step delay until wires intersect.

    :param wires:  List[str] of wire paths as strings
    :param start:  Tuple[int, int], start co-ordinate of wire
    """
    paths = [wire_str_to_path(_, start) for _ in wires]
    overlaps = [_ for _ in wire_get_overlaps(wires, start) if _ != start]
    overlap_times = []
    for overlap in overlaps:
        overlap_times.append(sum([_.index(overlap) for _ in paths]))
    return min(overlap_times)


def wire_get_shortest_overlap_distance(
    wires: List[str], start: Tuple[int, int] = (0, 0)
) -> int:
    """Return the distance from start point to the closest wire overlap.

    :param wires:  List[str] of wire paths as strings
    :param start:  Tuple[int, int], start co-ordinate of wire
    """
    overlaps = wire_get_overlaps(wires, start)
    distances = [(abs(x) + abs(y)) for (x, y) in overlaps if (x, y) != start]
    return min(distances)


def wire_get_overlaps(
    wires: List[str], start: Tuple[int, int] = (0, 0)
) -> List[Tuple[(int, int)]]:
    """Return overlap points between two or more wires.

    :param wires:  List[str] of wire paths as strings
    :param start:  Tuple[int, int], start co-ordinate of wire

    Returns the (x, y) locations where two or more wire paths overlap
    """
    paths = [wire_str_to_path(_, start) for _ in wires]
    overlaps = set(paths.pop())
    while len(paths) > 0:
        overlaps = overlaps.intersection(set(paths.pop()))
    return list(overlaps)


def wire_str_to_path(
    pathstr: str, start: Tuple[int, int] = (0, 0)
) -> List[Tuple[int, int]]:
    """Convert input wire path string to list of tuples of wire locations.

    :param pathstr:  str, wire path in, e.g. "R8,U5,L5,D3"
    :param start:  Tuple[int, int], start co-ordinate of wire

    Returns list of (x, y) tuples describing coordinates where the path is
    present, starting from `start`
    """
    parts = pathstr.split(",")
    locations = [start]
    for part in parts:
        way = part[0]
        val = int(part[1:])
        if way == "U":
            for _ in range(val):
                lastloc = locations[-1]
                locations.append((lastloc[0], lastloc[1] + 1))
        if way == "D":
            for _ in range(val):
                lastloc = locations[-1]
                locations.append((lastloc[0], lastloc[1] - 1))
        if way == "L":
            for _ in range(val):
                lastloc = locations[-1]
                locations.append((lastloc[0] - 1, lastloc[1]))
        if way == "R":
            for _ in range(val):
                lastloc = locations[-1]
                locations.append((lastloc[0] + 1, lastloc[1]))
    return locations
