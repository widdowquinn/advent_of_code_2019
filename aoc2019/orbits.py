# -*- coding: utf-8 -*-
"""Module to aid orbit calculations for Avent of Code 2019."""

from pathlib import Path
from typing import Generator, Iterable, List

import networkx as nx


def count_node_orbits(ograph: nx.DiGraph, node: str) -> int:
    """Return number of direct and indirect orbits of node around COM.

    :param ograph:  nx.DiGraph describing orbits
    :param node:  str, label of node
    """
    # len(_) - 1 because all_simple_paths() returns nodelist including "COM",
    #  rather than edges
    return sum([(len(_) - 1) for _ in nx.all_simple_paths(ograph, node, "COM")])


def count_orbital_transfers(ograph: nx.DiGraph, source: str, target: str) -> int:
    """Return minimum number of orbital transfers from `from` to `to` on ograph.

    :param ograph:  nx.DiGraph of orbits
    :param source:  str, source node
    :param target:  str, target node

    Input graph is a DiGraph, so convert to Graph, and find shortest path with nx
    """
    graph = ograph.to_undirected()
    # print(graph.nodes)
    sp = nx.shortest_path(graph, source, target)
    # print(sp)
    # Return len-1 because path of nodes is returned
    return len(sp) - 1


def count_orbits(ograph: nx.DiGraph) -> int:
    """Return number of direct and indirect orbits in the graph.

    :param ograph:  nx.DiGraph describing orbits

    Returns sum of indirect/direct orbits for each node in the graph that isn't
    the central COM node
    """
    return sum([count_node_orbits(ograph, _) for _ in ograph.nodes if _ != "COM"])


def network_from_orbits(orbits: Iterable) -> nx.DiGraph:
    """Return nx.DiGraph of passed orbits.

    :param orbits:  Iterable of orbits

    Given orbits [(A, B), (B, C)] returns DiGraph

    A <- B <- C
    """
    DG = nx.DiGraph()
    for orbit in orbits:
        # print(orbit)
        # print(orbit[1], orbit[0])
        DG.add_edge(orbit[1], orbit[0])
    return DG


def parse_orbits(defn: List[str]) -> Generator:
    """Parse string defining orbits.

    :param defn:  List[str] describing orbits

    Orbits are defined as:

    COM)B
    B)C
    C)D
    D)E

    and so on, where B orbits COM, C orbits B, etc.
    """
    for _ in defn:
        # print(_.split(")"))
        yield tuple(_.strip().split(")"))


def read_orbits(fname: Path):
    """Return list of (B, A) tuples where A orbits B.

    :param fname:  Path to file describing orbits

    """
    # Read orbits from file
    with fname.open("r") as ifh:
        orbits = parse_orbits(ifh.readlines())

    # Convert orbits to graph
    # print(orbits)
    orbit_graph = network_from_orbits(orbits)
    return orbit_graph
