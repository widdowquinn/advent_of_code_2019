# -*- coding: utf-8 -*-
"""Module for day 19 of Advent of Code 2019."""

from typing import Tuple

import matplotlib.pyplot as plt
import numpy as np

from .imachine import IntcodeMachine


class Drone(IntcodeMachine):

    """Intcode-powered agent that scans space."""

    def __init__(self, *args, **kwargs) -> None:
        """Set attributes unique to Drone."""
        super(Drone, self).__init__(*args, **kwargs)

        self.scan_result = None

    def deploy(self, x_val: int, y_val: int) -> int:
        """Return value at x, y co-ordinates."""
        output = self.run([x_val, y_val,])
        self.reset(hard_reset=True)
        return output

    def draw_scan(self) -> None:
        """Render image of scan."""
        plt.imshow(self.scan_result)

    def scan(self, val: int) -> np.array:
        """Scan space from (0,0) to (val, val)."""
        scan_result = np.zeros((val, val), np.int32)
        for row in range(val):
            for col in range(val):
                scan_result[row, col] = self.deploy(col, row)
        self.scan_result = scan_result

    def scan_region(self, x_start, x_end, y_start, y_end) -> np.array:
        """Scan space."""
        x_range = x_end - x_start + 1
        y_range = y_end - y_start + 1
        scan_result = np.zeros((y_range, x_range))
        for row in range(y_range):
            for col in range(x_range):
                scan_result[row, col] = self.deploy(col + x_start, row + y_start)
        self.scan_result = scan_result

    def track_beam_width(self, size: int, start_row=0) -> int:
        """Return first row where beam has width size."""
        row = start_row
        beam_width = 0
        while beam_width < size:
            row += 1
            signal = np.array([self.deploy(_, row) for _ in range(1000)])
            beam_width = np.sum(signal)
            print(row, beam_width)
        return row, beam_width, np.where(signal)

    def track_beam_height(self, size: int, start_col=0) -> int:
        """Return first row where beam has height size."""
        col = start_col
        beam_height = 0
        while beam_height < size:
            col += 1
            signal = np.array([self.deploy(col, _) for _ in range(1000)])
            beam_height = np.sum(signal)
            print(col, beam_height)
        return col, beam_height, np.where(signal)

    def track_beam_square(self, size: int, x_start: int) -> Tuple[int, int]:
        """Return upper-left co-ordinate of first point where a square of size fits.

        :param size:  size of square (side)
        :param x_start:  start column

        Track horizontal overlap between two vertical beams, separated by size.
        """
        overlap = set()  # start with empty overlap set
        size -= 1  # size offset
        x_val = x_start
        while len(overlap) < size:  # Do y-positions of sets overlap by minimum amount?
            x_val += 1  # Increment x position
            print(x_val)
            col1, col2 = x_val, x_val + size
            beam1 = np.where(np.array([self.deploy(col1, _) for _ in range(1000)]))[0]
            beam2 = np.where(np.array([self.deploy(col2, _) for _ in range(1000)]))[0]
            overlap = set(beam1).intersection(set(beam2))
        y_val = min(overlap)
        return x_val, y_val
